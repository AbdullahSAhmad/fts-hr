/**
 * Created by Khalid Ardah on 12/27/2018.
 */

"use strict";

const path    = require("path");
const webpack = require("webpack");

const config = require("./webpack.config");

const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const uglifyOptions  = {
    mangle: {
        except: ["$super", "$", "exports", "require", "angular"]
    },
    parallel: true
};

config.output = {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: ""
};

config.plugins.push(new UglifyJsPlugin(uglifyOptions));

module.exports = config;