module.exports = function(config) {
    config.set({

        basePath: "",

        files: [
            "node_modules/angular/angular.js",
            "node_modules/angular-mocks/angular-mocks.js",
            "node_modules/angular-route",
            "node_modules/angular-ui-router",
            "node_modules/angular-animate/angular-animate",
            "node_modules/angular-aria/angular-aria",
            "node_modules/angular-material/angular-material",
            "node_modules/ngstorage/ngStorage",
            "src/modules/core/core.module.js",
            "src/modules/holidays/holidays.module.js",
            "src/modules/holidays/forms/holidayForm/holidayForm.html",
            "src/modules/users/users.module.js",
            "src/modules/requests/requests.module.js",
            "src/modules/login/login.module.js",
            "src/modules/login/login.html",
            "src/modules/**/*.spec.js"
        ],

        // files to be browserified before test
        preprocessors: {
            "node_modules/ngstorage/ngStorage": ["webpack"],
            "src/modules/core/core.module.js": ["webpack"],
            "src/modules/holidays/holidays.module.js": ["webpack"],
            "src/modules/holidays/forms/holidayForm/holidayForm.html": ["ng-html2js"],
            "src/modules/users/users.module.js": ["webpack"],
            "src/modules/requests/requests.module.js": ["webpack"],
            "src/modules/login/login.module.js": ["webpack"],
            "src/modules/login/login.html": ["ng-html2js"],
            "src/modules/**/*.spec.js": ["webpack"],
        },

        browserify: {
            debug: true,
            transform: [["babelify"]],
        },

        specReporter: {
            maxLogLines: 5,             // limit number of lines logged per test
            suppressErrorSummary: true, // do not print error summary
            suppressFailed: false,      // do not print information about failed tests
            suppressPassed: false,      // do not print information about passed tests
            suppressSkipped: true,      // do not print information about skipped tests
            showSpecTiming: false,      // print the time elapsed for each spec
            failFast: false              // test would finish with error when a first fail occurs.
        },

        ngHtml2JsPreprocessor: {
            // strip this from the file path
            stripPrefix: "public/",
            stripSuffix: ".ext",
            // prepend this to the
            prependPrefix: "served/",

            // or define a custom transform function
            // - cacheId returned is used to load template
            //   module(cacheId) will return template at filepath
            cacheIdFromPath: function(filepath) {
                // example strips 'public/' from anywhere in the path
                // module(app/templates/template.html) => app/public/templates/template.html
                return filepath.substring(filepath.indexOf("/Scripts/angular-app/"));
            },

            // - setting this option will create only a single module that contains templates
            //   from all the files, so you can load them all with module('foo')
            // - you may provide a function(htmlPath, originalPath) instead of a string
            //   if you'd like to generate modules dynamically
            //   htmlPath is a originalPath stripped and/or prepended
            //   with all provided suffixes and prefixes
            moduleName: "ftsTestTemplates"
        },

        autoWatch: true,

        watchify: {
            poll: true
        },

        frameworks: ["jasmine", "browserify"],

        reporters: ["spec"],

        browsers: ["Chrome"],

        plugins: [
            "karma-chrome-launcher",
            "karma-firefox-launcher",
            "karma-jasmine",
            "karma-browserify",
            "karma-webpack",
            "karma-spec-reporter",
            "karma-ng-html2js-preprocessor"
        ]
    });
};