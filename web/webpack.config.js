/**
 * Created by Khalid Ardah on 12/27/2018.
 */

"use strict";

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const cleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const globImporter = require("node-sass-glob-importer");
const path = require("path");
const preprocess = require("preprocess");
const webpack = require("webpack");
const WebpackFilePreprocessorPlugin = require("webpack-file-preprocessor-plugin");

const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
const devtool = "source-map";
const preprocessContext = {
    ts: Date.now()
};

const entry = {
    index: [
        path.resolve(__dirname, "src/index.js")
    ]
};

const modules = {
    rules: [
        {
            test: /src.*\.js$/,
            exclude: [/node_modules/, /.*\.spec\.js/],
            use: [
                {
                    loader: "ng-annotate-loader"
                },
                {
                    loader: "babel-loader"
                }
            ]
        },
        {
            test: /src.*\.(s*)css$/,
            use: ExtractTextPlugin.extract([
                {
                    loader: "style-loader"
                },
                {
                    loader: "css-loader"
                },
                {
                    loader: "sass-loader",
                    options: {
                        importer: globImporter()
                    }
                }
            ])
        }
    ]
};

const plugins = [
    new ExtractTextPlugin({
        disable: process.env.NODE_ENV !== "production",
        filename: "[name].[contenthash].css"
    }),

    new HtmlWebpackPlugin({
        hash: true,
        inject: "body",
        template: "./index.html"
    }),

    // Automatically move all modules defined outside of application directory to vendor bundle.
    new CommonsChunkPlugin({
        minChunks: function (module) {
            return module.resource && module.resource.indexOf(path.resolve(__dirname, "src")) === -1;
        },
        name: "vendor"
    }),

    new WebpackFilePreprocessorPlugin({
        pattern: /index\.html$/,
        process: function (source) {
            return preprocess.preprocess(source.toString(), preprocessContext);
        }
    }),

    // copy html templates(should be included in raw-loader) and assets folder to dist
    new CopyWebpackPlugin([
        { from: path.resolve(__dirname, "assets"), to: "assets" },
        { from: "src/**/*.html" },
        { from: "js/**/*" },
        { from: "favicon.ico" },
        { from: "node_modules/angular-material/angular-material.css", to: "node_modules/angular-material/angular-material.css" }
    ]),

    new cleanWebpackPlugin(["dist"])
];

const resolve = {
    alias: {
        src: path.resolve(__dirname, "src"),
        assets: path.resolve(__dirname, "assets"),
        constants: path.resolve(__dirname, "src/shared/constants.js"),
        moment: path.resolve(__dirname, "node_modules/moment/moment.js"),
        js: path.resolve(__dirname, "js"),
        styleVars: path.resolve(__dirname, "src/styles/vars.scss")
    },
    descriptionFiles: ["package.json"],
    modules: ["node_modules"]
};

module.exports = {
    devtool: devtool,
    entry: entry,
    module: modules, // Set to not conflict with module from module.exports,
    plugins: plugins,
    resolve: resolve
};