/**
 * Created by Khalid Ardah on 12/27/2018.
 */

"use strict";

const path    = require("path");
const webpack = require("webpack");

const webpackConfig = require("./webpack.config");

const HotModuleReplacementPlugin = webpack.HotModuleReplacementPlugin; // Hot reloading and inline style replacement

webpackConfig.devServer = {
    allowedHosts: [
        "localhost"
    ],
    compress: true,
    contentBase: path.join(__dirname, ""),
    historyApiFallback: true,
    hot: true,
    inline: true,
    noInfo: true,
    port: 8080,
    public: "localhost:8080",
    watchContentBase: true,
    proxy: {
        "/auth": "http://localhost:63775",
        "/api": "http://localhost:63775"
    }
};

webpackConfig.devtool = "inline-source-map";

webpackConfig.output = {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "dev"),
    publicPath: "/"
};

webpackConfig.plugins.push(new HotModuleReplacementPlugin());

module.exports = webpackConfig;