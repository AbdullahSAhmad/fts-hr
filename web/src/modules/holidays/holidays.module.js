/**
 * Created by Khalid Ardah on 1/24/2018.
 */

/**
 * holidays module with its controllers
 */

const HolidaysController = require("./holidays.controller");
const HolidayFormController = require("./forms/holidayForm/holidayForm.controller");
const HolidaysListController = require("./list/holidaysList.controller");
const holidayFormService = require("./forms/holidayForm/holidayFormService");

/**
 * holidays Routes
 */
const holidaysRoutes = require("./holidays.route");

const holidaysModule = angular.module("holidaysModule", [])
    .controller("HolidaysController", HolidaysController)
    .controller("HolidaysListController", HolidaysListController)
    .controller("HolidayFormController", HolidayFormController)

    // holidays services
    .factory("holidayFormService", holidayFormService);

/**
 * Holidays authenticated routes
 */
holidaysModule.config(
    function ($stateProvider) {
        holidaysRoutes($stateProvider);
    }
);
