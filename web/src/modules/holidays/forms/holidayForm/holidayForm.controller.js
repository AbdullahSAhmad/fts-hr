/**
 * Created by Khalid Ardah on 1/24/2018.
 */


/**
 * holiday Form controller
 * @param holidayFormService
 */

holidayFormController.$inject = ["$scope", "holidayFormService", "moment", "$state", "holiday"];
function holidayFormController($scope, holidayFormService, moment, $state, holiday) {

    // Handler functions
    $scope.handlers = {
        submit: submit,
        deleteHoliday: deleteHoliday,
        close: close
    };

    activate();

    function activate() {
        // holiday default model for the form
        $scope.model = {
            date: moment().toDate(),
            name: "",
            repeatableEveryYear: false
        };

        // If in edit route
        if (holiday) {
            $scope.isEditMode = true;
            $scope.model = holiday;
            $scope.model.date = moment(holiday.date).toDate();
        }
    }

    function submit(holiday) {
        if ($scope.isEditMode) {
            holidayFormService.editHoliday(holiday).then(function success() {
                redirectToEdit(holiday.id);
            }, handleError);
        }
        else {
            holidayFormService.createHoliday(holiday).then(function success(data) {
                redirectToEdit(data.id);
            }, handleError);
        }
    }

    function deleteHoliday(id) {
        holidayFormService.deleteHoliday(id).then(function success() {
            redirectToList();
        }, handleError);
    }

    function close() {
        redirectToList();
    }

    function redirectToList() {
        $state.go("layout.holidays.list");
    }

    function redirectToEdit(id) {
        $state.go("layout.holidays.edit", {id: id});
    }

    function handleError(error) {
        $scope.focused = false;
        $scope.formErrorMessage = error.data.responseStatus.message;
    }
}

module.exports = holidayFormController;