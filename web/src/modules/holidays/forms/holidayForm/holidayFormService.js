/**
 * Created by Khalid Ardah on 1/24/2018.
 */


/**
 * holiday form service
 * Service name: holidayFormService
 * @param ftsHttpService
 */

holidayFormService.$inject = ["ftsHttpService", "CONSTANTS"];

function holidayFormService(ftsHttpService, CONSTANTS) {

    return {
        createHoliday: createHoliday,
        editHoliday: editHoliday,
        deleteHoliday: deleteHoliday
    };

    function createHoliday(holiday) {
        return ftsHttpService.post({url: CONSTANTS.API.HOLIDAYS.CREATE, data: holiday});
    }

    function editHoliday(holiday) {
        return ftsHttpService.put({url: CONSTANTS.API.HOLIDAYS.HOLIDAY + holiday.id, data: holiday});
    }

    function deleteHoliday(id) {
        return ftsHttpService.delete({url: CONSTANTS.API.HOLIDAYS.HOLIDAY + id});
    }
}

module.exports = holidayFormService;