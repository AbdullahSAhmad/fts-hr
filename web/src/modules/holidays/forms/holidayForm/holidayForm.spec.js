require("../../../../../node_modules/ngstorage/ngStorage");
require("../../../../../node_modules/angular-ui-router");
const moment = require("moment");
const CONSTANTS = require("../../../../shared/constants");

describe("HolidayFormController", function() {
    let $controller, controller, scope, $rootScope, $httpBackend, state, $compile, template;

    beforeEach(function() {
        angular.mock.module("ui.router");
        angular.mock.module("coreModule");

        angular.mock.module(function ($provide) {
            $provide.value("moment", moment);
            $provide.value("CONSTANTS", CONSTANTS);
        });
        angular.mock.module("ftsTestTemplates");

    });
    beforeEach(angular.mock.module("holidaysModule"));


    beforeEach(inject(function(_$controller_, _$rootScope_, _$httpBackend_, _$q_, _$state_, $templateCache, _$compile_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = _$state_;

        scope = $rootScope.$new();
        template = $templateCache.get("src/modules/holidays/forms/holidayForm/holidayForm.html");
        $compile = _$compile_;
        $compile(template)(scope);
        controller = $controller("HolidayFormController", { $scope: scope, holiday: undefined });
    }));

    it("Controller should be created", function() {
        expect(controller).toBeDefined();
    });

    describe("form validity", function() {
        it("valid form values", function() {
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
            scope.model = getHoliday();
            scope.$digest();
            expect(scope.form.$valid).toEqual(true);
        });

        it("empty form name", function() {
            scope.model = getHoliday();
            scope.model.name = "";
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
        });

        it("invalid form name (less than 3)", function() {
            scope.model = getHoliday();
            scope.model.name = "aa";
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
        });

        it("empty form date", function() {
            scope.model = getHoliday();
            scope.model.date = "";
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
        });
    });

    it("add valid holiday", function() {
        // fill data
        const model = getHoliday();

        scope.model = model;

        // expects add mode
        expect(scope.isEditMode).toBeUndefined();

        // spy on function request and expect to route to edit state
        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.holidays.edit");
        });

        // submit the values
        scope.handlers.submit(scope.model);

        // dummy response
        $httpBackend
            .when("POST", CONSTANTS.API.HOLIDAYS.LIST)
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();

        // added successfully then moved to edit state
        expect(state.go).toHaveBeenCalled();
    });

    it("edit holiday with success status", function() {

        let $scope = $rootScope.$new();
        $controller("HolidayFormController", { $scope: $scope, holiday: getHoliday() });

        spyOn(state, "go").and.callFake(function(stateParam, params) {
            // done successfully
            expect(stateParam).toEqual("layout.holidays.edit");
            expect(params.id).toEqual("1");
        });

        $scope.handlers.submit($scope.model);

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.HOLIDAYS.HOLIDAY + "1")
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("edit holiday with failure", function() {

        let $scope = $rootScope.$new();
        $controller("HolidayFormController", { $scope: $scope, holiday: getHoliday() });

        $scope.handlers.submit($scope.model);
        const message = "invalid date";

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.HOLIDAYS.HOLIDAY + "1")
            .respond(400, {
                status: "bad request",
                responseStatus: {message: message}
            });
        $httpBackend.flush();

        // should display a message
        expect($scope.focused).toBeFalsy();
        expect($scope.formErrorMessage).toEqual(message);

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("remove holiday successfully", function() {
        let $scope = $rootScope.$new();
        const holiday = getHoliday();
        $controller("HolidayFormController", { $scope: $scope, holiday: holiday });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // deleted successfully
            expect(stateParam).toEqual("layout.holidays.list");
        });

        $scope.handlers.deleteHoliday(holiday.id);

        // dummy response
        $httpBackend
            .when("DELETE", CONSTANTS.API.HOLIDAYS.HOLIDAY + "1")
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();
    });

    it("remove holiday with failure", function() {
        let $scope = $rootScope.$new();
        const holiday = getHoliday();
        $controller("HolidayFormController", { $scope: $scope, holiday: holiday });

        const message = "couldn't find holiday";
        $scope.handlers.deleteHoliday(holiday.id);

        // dummy response
        $httpBackend
            .when("DELETE", CONSTANTS.API.HOLIDAYS.HOLIDAY + "1")
            .respond(404, {
                status: "notFound",
                responseStatus: {message: message}
            });
        $httpBackend.flush();

        // should display a message
        expect($scope.focused).toBeFalsy();
        expect($scope.formErrorMessage).toEqual(message);
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
});

function getHoliday() {
    const holidayDate = moment().add(1, "year").toDate();

    const holiday = {
        id: "1",
        date: holidayDate,
        name: "test holiday",
        repeatableEveryYear: false
    };

    return holiday;
}