/**
 * Created by Khalid Ardah on 1/24/2018.
 */
const CONSTANTS = require("constants");
function dataSource(ftsHttpService, moment) {
    const options = new kendo.data.DataSource(
        {
            pageSize: 20,
            transport: {
                read: function(options) {
                    ftsHttpService.get({url: CONSTANTS.API.HOLIDAYS.LIST}).then(
                        function(result) {options.success(result.data);},
                        function(result) {options.error(result);}
                    );
                }
            },
            schema: {
                model: {
                    fields: {
                        name: { type: "string" },
                        date: { type: "date" },
                        repeatableEveryYear: { type: "boolean" }
                    }
                },
                parse: function(data) {
                    // Convert string date to date object
                    $.each(data, function(index, request) {
                        request.date = moment(request.date).toDate();
                    });
                    return data;
                }
            }
        }
    );

    return options;
}

module.exports = dataSource;
