/**
 * Created by Khalid Ardah on 1/24/2018.
 */

function usersListOptions(ftsHttpService, moment) {
    return {
        dataSource: require("./dataSourceOptions")(ftsHttpService, moment),
        height: 550,
        groupable: false,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [{
            field: "name",
            title: "Name",
            width: 340
        }, {
            field: "date",
            title: "Date",
            template: "{{getFormatedDate(dataItem.date)}}"
        }, {
            field: "repeatableEveryYear",
            title: "Repeatable Every Year",
            filterable: {
                messages: {
                    isTrue: "Repeatable",
                    isFalse: "NOT Repeatable"
                }
            },
            template: `{{dataItem.repeatableEveryYear? "Yes": "No"}}`
        }],
        filterable: {
            extra: false,
            operators: {
                string: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    contains: "Contains",
                    doesnotcontain: "Doesn't contain"
                },
                date: {
                    eq: "Equal",
                    neq: "Not equal",
                    gt: "After",
                    lt: "Before"
                }
            }
        }
    };
}
module.exports = usersListOptions;
