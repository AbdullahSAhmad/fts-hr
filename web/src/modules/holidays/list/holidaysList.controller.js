/**
 * Created by Khalid Ardah on 1/24/2018.
 */


/**
 * holidays List controller
 */

const holidaysGridOptions = require("./holidaysListGridOptions");

holidaysListController.$inject = ["$scope", "moment", "$state", "CONSTANTS", "ftsHttpService", "userService"];
function holidaysListController($scope, moment, $state, CONSTANTS, ftsHttpService, userService) {

    activate();

    function activate() {
        $scope.flags = {
            showAddBtn: userService.checkUserType(CONSTANTS.USER.EMPLOYEE) || userService.checkUserType(CONSTANTS.USER.MANAGER)
        };

        $scope.getFormatedDate = function(date) {
            return moment(date).format(CONSTANTS.FORMAT.DATE);
        };

        // Options for grid
        const options = holidaysGridOptions(ftsHttpService, moment);
        $scope.kendoComponents = {
            holidaysGrid: {
                options: options
            }
        };
        initDblclickHandler();
    }

    // Add double click functionality to the rows
    function initDblclickHandler() {
        $scope.$on("kendoWidgetCreated", function(event, widget){
            // Check if the widget is holidaysGrid
            if (widget === $scope.kendoComponents.holidaysGrid.reference) {
                widget.tbody.on("dblclick","tr[data-uid]", function () {
                    $state.go("layout.holidays.edit", {id: widget.dataItem(this).id});
                });
            }
        });
    }
}

module.exports = holidaysListController;