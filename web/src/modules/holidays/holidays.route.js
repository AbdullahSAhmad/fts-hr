/**
 * Created by Khalid Ardah on 1/24/2018.
 */

const CONSTANTS = require("constants");

const holidaysRoutes = function(stateProvider) {

    const listState = {
        url: "/holidays/list",
        templateUrl: "src/modules/holidays/list/holidaysList.html",
        controller: "HolidaysListController"
    };

    const editState = {
        url: "/holidays/edit/:id",
        templateUrl: "src/modules/holidays/forms/holidayForm/holidayForm.html",
        controller: "HolidayFormController",
        resolve: {
            holiday: getHoliday
        },
        // Close side nav when enter
        onEnter: onEnter,
        // Open side nav when exit
        onExit: onExit
    };

    const addState = {
        url: "/holidays/add",
        templateUrl: "src/modules/holidays/forms/holidayForm/holidayForm.html",
        controller: "HolidayFormController",
        resolve: {
            holiday: function() {
                return undefined;
            },
        },
        // Close side nav when enter
        onEnter: onEnter,
        // open side nav when exit
        onExit: onExit
    };

    stateProvider.state("layout.holidays.list", listState);
    stateProvider.state("layout.holidays.edit", editState);
    stateProvider.state("layout.holidays.add", addState);

};

getHoliday.$inject = ["ftsHttpService", "$stateParams", "$state", "$timeout"];
function getHoliday(ftsHttpService, $stateParams, $state, $timeout) {
    // if id is empty go to holidays list
    if (!$stateParams.id) {
        $timeout(function() {$state.go("layout.holidays.list");});
        return;
    }

    return ftsHttpService.get({url: CONSTANTS.API.HOLIDAYS.HOLIDAY + $stateParams.id})
        .then (
            function success(response) {
                return response.data;
            },
            function error() {
                $timeout(function() {$state.go("layout.holidays.list");});
            });
}

onEnter.$inject = ["sideNavService"];
function onEnter(sideNavService){
    sideNavService.openSideNav(false);
}

onExit.$inject = ["sideNavService"];
function onExit(sideNavService){
    sideNavService.openSideNav(true);
}
module.exports = holidaysRoutes;