const CONSTANTS = require("constants");
module.exports = [
    { title: "Dashboard", state: "layout.dashboard", notAuth: [CONSTANTS.USER.EMPLOYEE] },
    { title: "Users", state: "layout.users.list", notAuth: [CONSTANTS.USER.EMPLOYEE] },
    { title: "Requests", state: "layout.requests.list" },
    { title: "Holidays", state: "layout.holidays.list" }
];