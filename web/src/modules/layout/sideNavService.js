/**
 * Created by Khalid Ardah on 1/16/2018.
 */


/**
 * Side Nav service for openning and closing it
 * Service name: sideNavService
 */

sideNavService.$inject = [];

function sideNavService() {

    const service = {
        openSideNav: openSideNav,
        sideNavOpened: true
    };

    return service;

    function openSideNav(opened) {
        service.sideNavOpened = opened;
        return ;
    }
}

module.exports = sideNavService;