/**
 * Created by Khalid Ardah on 1/6/2018.
 */

const layoutRoutes = function(stateProvider) {

    const dashboardState = {
        url: "/dashboard",
        templateUrl: "src/modules/dashboard/dashboard.html",
        controller: "DashboardController"
    };

    const usersState = {
        abstract: true,
        url: "",
        templateUrl: "src/modules/users/users.html",
        controller: "UsersController"
    };

    const requestsState = {
        abstract: true,
        url: "",
        templateUrl: "src/modules/requests/requests.html",
        controller: "RequestsController"
    };

    const holidaysState = {
        abstract: true,
        url: "",
        templateUrl: "src/modules/holidays/holidays.html",
        controller: "HolidaysController"
    };

    stateProvider.state("layout.dashboard", dashboardState);
    stateProvider.state("layout.users", usersState);
    stateProvider.state("layout.requests", requestsState);
    stateProvider.state("layout.holidays", holidaysState);

};

module.exports = layoutRoutes;