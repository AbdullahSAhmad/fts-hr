/**
 * Created by Khalid Ardah on 1/7/2018.
 */

const menuItems = require("./menuItems");

/**
 * MainLayout controller
 */
layoutController.$inject = ["$scope", "sideNavService", "userService", "_", "authService", "$state"];

function layoutController($scope, sideNavService, userService, _, authService, $state) {
    $scope.handlers = {
        logout: logout
    };

    // Init view
    activate();

    function activate() {

        // sideNav menu items
        $scope.menuItems = menuItems;
        $scope.sideNavService = sideNavService;
        $scope.user = userService.getUser();
        $scope._ = _;
    }

    function logout() {
        authService.logout().finally(function(response) {
            $state.go("login");
            return response;
        });
    }
}

module.exports = layoutController;