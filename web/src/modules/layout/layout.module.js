/**
 * Created by Khalid Ardah on 1/6/2018.
 */

/**
 * Layout module with its controllers
 */

const layoutController = require("./layout.controller");
const sideNavService = require("./sideNavService");

/**
 * Layout Routes
 */
const layoutRoutes = require("./layout.route");

const layoutModule = angular.module("layoutModule", ["dashboardModule", "usersModule", "requestsModule", "holidaysModule"])
    .controller("LayoutController", layoutController)

    // Services
    .factory("sideNavService", sideNavService);

/**
 * App authenticated routes
 */
layoutModule.config(
    function ($stateProvider) {

        // app sub routes
        layoutRoutes($stateProvider);
    }
);
