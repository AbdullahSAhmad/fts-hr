/**
 * Created by Khalid Ardah on 12/27/2018.
 */


/**
 * Login controller
 */
loginController.$inject = ["$scope", "authService", "$state"];

function loginController($scope, authService, $state) {
    // Init view
    activate();

    $scope.handlers = {
        login: login
    };

    function activate() {
        // Form model
        $scope.model = {
            userName: "",
            password: ""
        };
    }


    function login(user) {
        authService.loginUser(user).then(
            function success() {
                $state.go("layout.dashboard");
            },
            function error() {
                // Display message for user
                $scope.focused = false;
            }
        );
    }
}

module.exports = loginController;