require("../../../node_modules/ngstorage/ngStorage");
require("../../../node_modules/angular-ui-router");
const CONSTANTS = require("../../shared/constants");

describe("LoginController", function() {

    let $controller, controller, scope, $rootScope, $httpBackend, $q, state, template, $compile;

    beforeEach(function() {
        angular.mock.module("ui.router");
        angular.mock.module("coreModule");
        angular.mock.module(function ($provide) {
            $provide.value("CONSTANTS", CONSTANTS);
        });
        angular.mock.module("ftsTestTemplates");
    });

    beforeEach(angular.mock.module("loginModule"));

    beforeEach(inject(function(_$controller_, _$rootScope_, _$httpBackend_, _$q_, _$state_, _$compile_, $templateCache) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;
        state = _$state_;

        scope = $rootScope.$new();
        template = $templateCache.get("src/modules/login/login.html");
        $compile = _$compile_;
        $compile(template)(scope);
        controller = $controller("LoginController", { $scope: scope });
    }));

    it("Controller should be created", function() {
        expect(controller).toBeDefined();
    });

    describe("form validity", function() {
        it("valid form values", function() {
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
            scope.model = getUserCredentials();
            scope.$digest();
            expect(scope.form.$valid).toEqual(true);
        });

        it("empty form userName", function() {
            scope.model = getUserCredentials();
            scope.model.userName = "";
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
        });

        it("invalid form userName (less than 3)", function() {
            scope.model = getUserCredentials();
            scope.model.userName = "aa";
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
        });

        it("empty form password", function() {
            scope.model = getUserCredentials();
            scope.model.password = "";
            scope.$digest();
            expect(scope.form.$valid).toEqual(false);
        });
    });

    it("Login with good credentials", function() {
        // spy on function request
        spyOn(state, "go").and.returnValue($q.when("ok"));

        // login
        scope.handlers.login({userName: "test", password: "test"});

        // dummy response
        $httpBackend
            .when("POST", CONSTANTS.API.LOGIN)
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();

        // success, expect the state.go to be called
        expect(state.go).toHaveBeenCalled();
    });

    it("Login with bad credentials", function() {
        spyOn(state, "go").and.returnValue($q.when("failed"));
        scope.handlers.login({userName: "test", password: "aa"});
        $httpBackend
            .when("POST", CONSTANTS.API.LOGIN)
            .respond(401, {
                status: "Unauthorized"
            });
        $httpBackend.flush();

        // when $scope.focused is false this displays the error message to the user
        expect(scope.focused).toBeFalsy();
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
});

function getUserCredentials() {
    return {
        userName: "aaaa",
        password: "aaaa"
    };
}