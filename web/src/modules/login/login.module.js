/**
 * Created by Khalid Ardah on 12/27/2018.
 */

const loginController = require("./login.controller");
angular.module("loginModule", []).controller("LoginController", loginController);
