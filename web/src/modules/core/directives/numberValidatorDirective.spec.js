require("../../../../node_modules/ngstorage/ngStorage");

describe("numberValidatorDirective default(positive or negative)", function() {
    let $scope, form;
    beforeEach(function() {
        angular.mock.module("coreModule");
    });
    beforeEach(inject(function($compile, $rootScope) {
        $scope = $rootScope;
        $scope.model = "";
        const element = angular.element(
            "<form name=\"form\"><input ng-model=\"model\" name=\"model\" number /></form>"
        );

        // apply the template against the scope
        $compile(element)($scope);
        form = $scope.form;
    }));

    describe("valid values", function() {
        it("passed nothing", function() {
            applyTest("", true);
        });
        it("number", function() {
            applyTest(3, true);
            applyTest(-3, true);
        });
        it("2 digit number or more", function() {
            applyTest(33, true);
            applyTest(-33, true);
            applyTest(22145, true);
            applyTest(-22145, true);
        });
    });

    describe("invalid values", function() {
        it("not number", function() {
            applyTest(" ", false);
            applyTest("*", false);
            applyTest("t", false);
            applyTest("A", false);
            applyTest("Haaa", false);
            applyTest("$%@#", false);
            applyTest("AA%$#SSF", false);
            applyTest("test", false);
            applyTest("TEST", false);
            applyTest("TeST", false);
            applyTest("3-3", false);
        });
        it("mixed", function() {
            applyTest("1t", false);
            applyTest("1test", false);
            applyTest("t1", false);
            applyTest("test1", false);
            applyTest("te 1tes", false);
            applyTest("1t1", false);
            applyTest("t1%t1t1", false);
            applyTest("t1t1t1", false);
        });
    });

    function applyTest(value, result) {
        form.model.$setViewValue(value);
        $scope.$digest();
        expect(form.model.$valid).toBe(result);
    }
});

describe("numberValidatorDirective positive", function() {
    let $scope, form;
    beforeEach(function() {
        angular.mock.module("coreModule");
    });
    beforeEach(inject(function($compile, $rootScope) {
        $scope = $rootScope;
        $scope.model = "";
        const element = angular.element(
            "<form name=\"form\"><input ng-model=\"model\" name=\"model\" number=\"positive\" /></form>"
        );

        // apply the template against the scope
        $compile(element)($scope);
        form = $scope.form;
    }));

    describe("valid values", function() {
        it("passed nothing", function() {
            applyTest("", true);
        });
        it("number", function() {
            applyTest(3, true);
        });
        it("2 digit number or more", function() {
            applyTest(33, true);
            applyTest(22145, true);
        });
    });

    describe("invalid values", function() {
        it("not number", function() {
            applyTest(" ", false);
            applyTest("*", false);
            applyTest("t", false);
            applyTest("A", false);
            applyTest("Haaa", false);
            applyTest("$%@#", false);
            applyTest("AA%$#SSF", false);
            applyTest("test", false);
            applyTest("TEST", false);
            applyTest("TeST", false);
            applyTest("3-3", false);
        });
        it("mixed", function() {
            applyTest("1t", false);
            applyTest("1test", false);
            applyTest("t1", false);
            applyTest("test1", false);
            applyTest("te 1tes", false);
            applyTest("1t1", false);
            applyTest("t1%t1t1", false);
            applyTest("t1t1t1", false);
        });

        it("negative", function() {
            applyTest(-33, false);
            applyTest(-3, false);
            applyTest(-22145, false);
        });
    });

    function applyTest(value, result) {
        form.model.$setViewValue(value);
        $scope.$digest();
        expect(form.model.$valid).toBe(result);
    }
});

describe("numberValidatorDirective negative", function() {
    let $scope, form;
    beforeEach(function() {
        angular.mock.module("coreModule");
    });
    beforeEach(inject(function($compile, $rootScope) {
        $scope = $rootScope;
        $scope.model = "";
        const element = angular.element(
            "<form name=\"form\"><input ng-model=\"model\" name=\"model\" number=\"negative\" /></form>"
        );

        // apply the template against the scope
        $compile(element)($scope);
        form = $scope.form;
    }));

    describe("valid values", function() {
        it("passed nothing", function() {
            applyTest("", true);
        });
        it("number", function() {
            applyTest(-3, true);
        });
        it("2 digit number or more", function() {
            applyTest(-33, true);
            applyTest(-22145, true);
        });
    });

    describe("invalid values", function() {
        it("not number", function() {
            applyTest(" ", false);
            applyTest("*", false);
            applyTest("t", false);
            applyTest("A", false);
            applyTest("Haaa", false);
            applyTest("$%@#", false);
            applyTest("AA%$#SSF", false);
            applyTest("test", false);
            applyTest("TEST", false);
            applyTest("TeST", false);
            applyTest("3-3", false);
        });
        it("mixed", function() {
            applyTest("1t", false);
            applyTest("1test", false);
            applyTest("t1", false);
            applyTest("test1", false);
            applyTest("te 1tes", false);
            applyTest("1t1", false);
            applyTest("t1%t1t1", false);
            applyTest("t1t1t1", false);
        });

        it("positive", function() {
            applyTest(33, false);
            applyTest(3, false);
            applyTest(22145, false);
        });
    });

    function applyTest(value, result) {
        form.model.$setViewValue(value);
        $scope.$digest();
        expect(form.model.$valid).toBe(result);
    }
});
