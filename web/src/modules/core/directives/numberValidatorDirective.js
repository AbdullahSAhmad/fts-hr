/**
 * Created by Khalid Ardah on 1/9/2018.
 */

/**
 * Directive name: number
 */
module.exports = [
    function() {
        return {
            require: "ngModel",
            restrict: "A",
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.number = function(modelValue, viewValue) {

                    let valid = true;

                    if (viewValue) {
                        let regex = /^[-+]?\d+$/;

                        if(attrs.number === "negative") {
                            regex = /^-\d+$/;
                        }
                        else if (attrs.number === "positive") {
                            regex = /^[+]?\d+$/;
                        }

                        return !!regex.test(viewValue);
                    }

                    return valid;
                };
            }
        };
    }
];