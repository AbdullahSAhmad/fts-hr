/**
 * Created by Khalid Ardah on 1/9/2018.
 */

/**
 * Directive name: alphanumeric
 */
module.exports = [
    function() {
        return {
            require: "ngModel",
            restrict: "A",
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.alphanumeric = function(modelValue, viewValue) {

                    let valid = true;

                    if (viewValue) {
                        let regex = /^\w*$/;

                        if(attrs.alphanumeric === "true") {
                            regex = /^[\w ]*$/;
                        }

                        return !!regex.test(viewValue);
                    }

                    return valid;
                };
            }
        };
    }
];