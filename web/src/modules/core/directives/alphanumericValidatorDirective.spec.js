require("../../../../node_modules/ngstorage/ngStorage");

describe("alphanumericValidatorDirective", function() {
    var $scope, form;
    beforeEach(function() {
        angular.mock.module("coreModule");
    });
    beforeEach(inject(function($compile, $rootScope) {
        $scope = $rootScope;
        var element = angular.element(
            "<form name=\"form\"><input ng-model=\"model\" name=\"model\" alphanumeric /></form>"
        );
        $scope.model = "";

        // apply the template against the scope
        $compile(element)($scope);
        form = $scope.form;
    }));

    describe("valid values", function() {
        it("passed nothing", function() {
            applyTest("", true);
        });
        it("integer", function() {
            applyTest(3, true);
        });
        it("2 digit integer or more", function() {
            applyTest(33, true);
            applyTest(22145, true);
        });
        it("1 char.", function() {
            applyTest("t", true);
        });
        it("more than one char.", function() {
            applyTest("tt", true);
            applyTest("test", true);
            applyTest("TEST", true);
            applyTest("TeST", true);
        });
        it("mixed(chars. and nums.)", function() {
            applyTest("1t", true);
            applyTest("1test", true);
            applyTest("t1", true);
            applyTest("test1", true);
            applyTest("te1tes", true);
            applyTest("1t1", true);
            applyTest("t1t1t1", true);
        });
    });

    describe("invalid values", function() {
        it("not alpha. char.", function() {
            applyTest(" ", false);
            applyTest("*", false);
        });
        it("mixed invalid chars. with valid one", function() {
            applyTest(" test", false);
            applyTest("t*est", false);
            applyTest("t*123", false);
            applyTest("4*123", false);
            applyTest("test$", false);
            applyTest("t$%%$#$^%&AAAASDAD", false);
        });
    });

    function applyTest(value, result) {
        form.model.$setViewValue(value);
        $scope.$digest();
        expect(form.model.$valid).toBe(result);
    }
});

