/**
 * Created by Khalid Ardah on 1/16/2018.
 */

/**
 * Directive name: passwordVerify
 */
module.exports = [
    function() {
        return {
            require: "ngModel",
            restrict: "A",
            scope: {
                passwordVerify: "="
            },
            link: function(scope, element, attrs, ctrl) {
                ctrl.$validators.passwordVerify = function(modelValue, viewValue) {
                    return scope.passwordVerify === viewValue;
                };
                scope.$watch("passwordVerify", function() {
                    ctrl.$validate();
                });
            }
        };
    }
];