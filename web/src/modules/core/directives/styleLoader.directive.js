/**
 * Usage: add it to core for lazy loading of styles while routing
 * .directive("head", directive code below....)
 */

module.exports = ["$rootScope","$compile",
    function($rootScope, $compile){
        return {
            restrict: "E",
            link: function(scope, elem) {
                const html = "<link rel='stylesheet' ng-repeat='cssUrl in routeStyles' ng-href='{{cssUrl}}' />";
                elem.append($compile(html)(scope));
                scope.routeStyles = [];
                $rootScope.$on("$routeChangeStart", function (e, next, current) {
                    if(current && current.$$route && current.$$route.css) {
                        if(!angular.isArray(current.$$route.css)) {
                            current.$$route.css = [current.$$route.css];
                        }
                        scope.routeStyles = [];
                    }
                    if(next && next.$$route && next.$$route.css) {
                        if(!angular.isArray(next.$$route.css)) {
                            next.$$route.css = [next.$$route.css];
                        }
                        angular.forEach(next.$$route.css, function(sheet) {
                            scope.routeStyles.push(sheet);
                        });
                    }
                });
            }
        };
    }
];