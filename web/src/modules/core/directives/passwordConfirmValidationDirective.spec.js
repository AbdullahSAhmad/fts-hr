require("../../../../node_modules/ngstorage/ngStorage");

describe("passwordConfirmValidationDirective", function() {
    var $scope, form;
    beforeEach(function() {
        angular.mock.module("coreModule");
    });
    beforeEach(inject(function($compile, $rootScope) {
        $scope = $rootScope;
        var element = angular.element(
            `<form name="form">
            <input required type="password" ng-model="model.newPassword" name="newPassword"/>
            <input required type="password" ng-model="model.passwordConfirm" name="passwordConfirm" data-password-verify="model.newPassword" />
            </form>`
        );
        $scope.model = {
            newPassword: "",
            passwordConfirm: ""
        };

        // apply the template against the scope
        $compile(element)($scope);
        form = $scope.form;
    }));

    it("matched values", function() {
        form.newPassword.$setViewValue("test");
        form.passwordConfirm.$setViewValue("test");
        $scope.$digest();
        expect(form.passwordConfirm.$valid).toBe(true);
    });

    it("not matched values", function() {
        form.newPassword.$setViewValue("test");
        form.passwordConfirm.$setViewValue("teest");
        $scope.$digest();
        expect(form.passwordConfirm.$valid).toBe(false);
    });

    it("matched then not matched", function() {
        form.newPassword.$setViewValue("test");
        form.passwordConfirm.$setViewValue("test");
        $scope.$digest();
        expect(form.passwordConfirm.$valid).toBe(true);

        form.newPassword.$setViewValue("teest");
        $scope.$digest();
        expect(form.passwordConfirm.$valid).toBe(false);

        form.newPassword.$setViewValue("test");
        $scope.$digest();

        form.passwordConfirm.$setViewValue("teest");
        $scope.$digest();
        expect(form.passwordConfirm.$valid).toBe(false);
    });
});

