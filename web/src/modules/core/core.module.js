const authService = require("./services/authService");
const userService = require("./services/userService");
const ftsHttpService = require("./services/ftsHttpService/ftsHttpService");
const httpAuthInterceptor = require("./services/ftsHttpService/httpAuthInterceptor");
const numberDirective = require("./directives/numberValidatorDirective");
const alphanumericDirective = require("./directives/alphanumericValidatorDirective");
const passwordConfirmDirective = require("./directives/passwordConfirmValidationDirective");

angular.module("coreModule", ["ngStorage"])
    .config(["$httpProvider", function($httpProvider) {

        //Add interceptors to http
        $httpProvider.interceptors.push("httpAuthInterceptor");
    }])

    //Core Services
    .factory("ftsHttpService", ftsHttpService)
    .factory("userService", userService)
    .factory("authService", authService)

    // Register interceptor for authentication
    .factory("httpAuthInterceptor", httpAuthInterceptor)

    // Directives
    .directive("number", numberDirective)
    .directive("alphanumeric", alphanumericDirective)
    .directive("passwordVerify", passwordConfirmDirective);