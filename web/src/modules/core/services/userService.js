/**
 * Created by Khalid Ardah on 1/27/2018.
 */

/**
 * user service for logged in user
 *
 * Service name: userService
 *
 * @param $localStorage
 */

userService.$inject = ["$localStorage"];

function userService($localStorage) {

    return {
        setUser: setUser,
        getUser: getUser,
        deleteUser: deleteUser,
        checkUserType: checkUserType
    };

    /**
   * Sets the logged in user
   */
    function setUser(user) {
        $localStorage.user = user;
    }

    /**
     * Returns the logged in user
     */
    function getUser() {
        return $localStorage.user;
    }

    /**
     * Removes local storage data
     */
    function deleteUser() {
        delete $localStorage.user;
    }

    /**
     * Takes user type and compare it with logged in user type
     * @param {*} type
     */
    function checkUserType(type) {
        const user = getUser();
        return user && user.meta && user.meta.type === type;
    }
}

module.exports = userService;