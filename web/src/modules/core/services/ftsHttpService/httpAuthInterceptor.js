

httpAuthInterceptor.$inject = ["userService", "$q", "$injector"];

function httpAuthInterceptor(userService, $q, $injector) {
    return {
        // add auth headers
        "request": function(config) {

            const user = userService.getUser();
            if(user && user.BearerToken) {
                config.headers.Authorization = "Bearer " + user.BearerToken;
            }

            config.headers["Content-Type"] = "application/json";
            return config;
        },

        // Redirect if unauthorized
        "responseError": function(rejection) {

            // Unauthorized user
            if (rejection.status === 401) {
                userService.deleteUser();
                // If used $state as dependency, this will cause circular dependance exception
                $injector.get("$state").go("login");
            }
            return $q.reject(rejection);
        }
    };
}

module.exports = httpAuthInterceptor;