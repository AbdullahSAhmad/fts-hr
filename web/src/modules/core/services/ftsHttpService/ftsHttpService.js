/**
 * Created by Khalid Ardah on 1/7/2018.
 */

const defaultHttpOptions = require("./httpOptions");

/**
 * Http service wrapper for server side requests customization
 *
 * Service name: ftsHttpService
 *
 * @param $http
 */

ftsHttpService.$inject = ["$http"];

function ftsHttpService($http) {

    return {
        get: get,
        post: post,
        put: put,
        patch: patch,
        delete: httpDelete
    };

    /**
   * Get Req.
   * @param url for api
   * @returns {HttpPromise|*}
   */
    function get(options) {
        options.method = "GET";
        options = extendOptions(defaultHttpOptions, options);
        return $http(options);
    }

    /**
   * Post Req.
   * @param url for api
   * @param data as json
   * @returns {HttpPromise|*}
   */
    function post(options) {
        options.method = "POST";
        options = extendOptions(defaultHttpOptions, options);
        return $http(options);
    }


    /**
   * Put Req. for edit
   * @param url for api
   * @param data as json
   * @returns {HttpPromise|*}
   */
    function put(options) {
        options.method = "PUT";
        options = extendOptions(defaultHttpOptions, options);
        return $http(options);
    }

    /**
   * Patch Req. for edit
   * @param url for api
   * @param data as json
   * @returns {HttpPromise|*}
   */
    function patch(options) {
        options.method = "PATCH";
        options = extendOptions(defaultHttpOptions, options);
        return $http(options);
    }

    /**
   * Delete Req.
   * @param url for api
   * @returns {HttpPromise|*}
   */
    function httpDelete(options) {
        options.method = "DELETE";
        options = extendOptions(defaultHttpOptions, options);
        return $http(options);
    }

    /**
     * Adds default config options of http
     * @param options options to be used
     * @returns same options passed but with filling missed default configs
     */
    function extendOptions(defaultHttpOptions, options) {
        return angular.extend({}, defaultHttpOptions, options);
    }
}

module.exports = ftsHttpService;