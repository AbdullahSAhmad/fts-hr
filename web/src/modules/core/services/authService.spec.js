require("../../../../node_modules/ngstorage/ngStorage");
const CONSTANTS = require("../../../shared/constants");

describe("authService", function() {
    let $httpBackend, authService, userService;

    beforeEach(function() {
        angular.mock.module("coreModule");
        angular.mock.module(function ($provide) {
            $provide.value("CONSTANTS", CONSTANTS);
        });
    });

    beforeEach(inject(function(_$httpBackend_, _authService_, _userService_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $httpBackend = _$httpBackend_;
        authService = _authService_;
        userService = _userService_;
    }));

    it("Login with valid credentials", function() {
        authService.loginUser({userName: "test", password: "test"});
        $httpBackend
            .when("POST", CONSTANTS.API.LOGIN)
            .respond(200, {
                userName: "test",
                userId: 2,
                bearerToken: "abc"
            });
        $httpBackend.flush();

        // expects to be logged in
        expect(authService.isLoggedIn()).toEqual(true);
    });

    it("Login with invalid credentials", function() {
        authService.loginUser({userName: "test", password: "teeest"});
        $httpBackend
            .when("POST", CONSTANTS.API.LOGIN)
            .respond(400, {
                status: "bar request"
            });
        $httpBackend.flush();

        // expects to be logged in
        expect(authService.isLoggedIn()).toEqual(false);
    });

    it("Login with valid credentials then logout", function() {
        authService.loginUser({userName: "test", password: "test"});
        $httpBackend
            .when("POST", CONSTANTS.API.LOGIN)
            .respond(200, {
                userName: "test",
                userId: 2,
                bearerToken: "abc"
            });
        $httpBackend.flush();

        // expects to be logged in
        expect(authService.isLoggedIn()).toEqual(true);

        authService.logout();

        $httpBackend
            .when("POST", CONSTANTS.API.LOGOUT)
            .respond(200, {});
        $httpBackend.flush();

        // expected to be logged out
        expect(authService.isLoggedIn()).toEqual(false);
    });

    it("Login with valid credentials and check user stored", function() {
        const user = {userName: "test", password: "test"};
        authService.loginUser(user);
        $httpBackend
            .when("POST", CONSTANTS.API.LOGIN)
            .respond(200, user);
        $httpBackend.flush();

        // expects to be stored
        expect(userService.getUser()).toEqual(user);
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
});
