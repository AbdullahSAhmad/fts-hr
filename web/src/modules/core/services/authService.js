/**
 * Created by Khalid Ardah on 1/7/2018.
 */

/**
 * Auth service for logging in, checking auth, and logging out
 *
 * Service name: authService
 *
 * @param ftsHttp
 * @param userService
 */

authService.$inject = ["ftsHttpService", "userService", "CONSTANTS"];

function authService(ftsHttpService, userService, CONSTANTS) {

    return {
        loginUser: loginUser,
        isLoggedIn: isLoggedIn,
        logout: logout
    };

    /**
   * Login Req. for auth
   * @param user: {username, password}
   */
    function loginUser(user) {
        return ftsHttpService.post({url: CONSTANTS.API.LOGIN, data: {userName: user.userName, password: user.password}}).then(
            function(response) {
                // Store user
                userService.setUser(response.data);
                return response;
            }
        );
    }

    /**
     * Checks if the user is logged in
     */
    function isLoggedIn() {
        // This is temp login check
        const user = userService.getUser();
        return ( !!user && !!user.bearerToken );
    }

    /**
     * Removes local storage data
     */
    function logout() {
        return ftsHttpService.post({url: CONSTANTS.API.LOGOUT}).finally(
            function(response) {
                // Delete user
                userService.deleteUser();
                return response;
            }
        );
    }
}

module.exports = authService;