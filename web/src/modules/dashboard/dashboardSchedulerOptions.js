/**
 * Created by Khalid Ardah on 1/27/2018.
 */

const dashboardDataSource = require("./dataSourceOptions");
const moment = require("moment");

function dashboardSchedulerOptions(ftsHttpService, $q) {
    // Setup start time of the day
    const startTime = moment();
    startTime.hours(8);
    startTime.minutes(0);
    startTime.seconds(0);

    // Setup the date to start with
    const date = startTime.clone();
    date.hours(0);

    const workDayEnd = startTime.clone();
    workDayEnd.hours(17);

    const workDayStart = startTime.clone();

    return {
        date: date.toDate(),
        startTime: startTime.toDate(),
        workDayStart: workDayStart.toDate(),
        workDayEnd: workDayEnd.toDate(),
        height: "100%",
        views: [
            "day",
            // { type: "workWeek" },
            "week",
            "agenda",
            { type: "month", selected: true }
        ],
        dataSource: dashboardDataSource(ftsHttpService, $q),
        editable: false,
        resources: [
            {
                field: "ownerId",
                title: "Owner",
                dataSource: [
                    { value: 1, color: "#CD6A4D" },
                    { value: 20, color: "#37403E" },
                    { value: 21, color: "#4CBEA4" },
                    { value: 3, color: "#BEDB84" }
                ]
            }
        ]
    };
}
module.exports = dashboardSchedulerOptions;