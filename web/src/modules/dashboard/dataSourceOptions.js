/**
 * Created by Khalid Ardah on 1/27/2018.
 */

const CONSTANTS = require("constants");
const birthdayParser = require("./parsers/birthdayParser");
const holidayParser = require("./parsers/holidayParser");
const requestParser = require("./parsers/requestParser");

function dataSourceOptions(ftsHttpService, $q) {
    return {
        batch: true,
        transport: {
            read: function(options) {
                $q.all([
                    ftsHttpService.get({url: CONSTANTS.API.HOLIDAYS.LIST}),
                    ftsHttpService.get({url: CONSTANTS.API.REQUESTS.LIST}),
                    ftsHttpService.get({url: CONSTANTS.API.BIRTHDAYS.LIST})
                ]).then(function success(result) {
                    const holidays = holidayParser(((result[0] || {}).data));
                    const requests = requestParser(((result[1] || {}).data));
                    const birthdays = birthdayParser(((result[2] || {}).data));

                    options.success(holidays.concat(requests, birthdays));
                }, function error(error) {options.error(error);});
            }
        },
        schema: {
            model: {
                fields: {
                    title: { from: "title", defaultValue: "No title" },
                    start: { type: "date", from: "start" },
                    end: { type: "date", from: "end" },
                    description: { from: "description", type: "string"},
                    ownerId: { from: "ownerId", defaultValue: 0 },
                    isAllDay: { type: "boolean", from: "IsAllDay" }
                }
            }
        }
    };
}

module.exports = dataSourceOptions;