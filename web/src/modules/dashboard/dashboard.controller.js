/**
 * Created by Khalid Ardah on 1/7/2018.
 */


/**
 * Dashboard controller
 */
const schedulerOptions = require("./dashboardSchedulerOptions");

dashboardController.$inject = ["$scope", "ftsHttpService", "$q"];
function dashboardController($scope, ftsHttpService, $q) {
    $scope.schedulerOptions = schedulerOptions(ftsHttpService, $q);
}

module.exports = dashboardController;
