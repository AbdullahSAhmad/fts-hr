/**
 * Created by Khalid Ardah on 1/27/2018.
 */

/**
 * dashboard module with its controller
 */

const DashboardController = require("./dashboard.controller");

angular.module("dashboardModule", [])
    .controller("DashboardController", DashboardController);