/**
 * Created by Khalid Ardah on 1/29/2018.
 */

const getDefaultEvent = require("./defaultEventObject");
const moment = require("moment");

/**
 * Parses json of holiday type into data source json for schedular
 * @param holidays array of holidays
 */
function parser(holidays) {
    if(!holidays) {
        return [];
    }

    // Array of holidays
    return holidays.map(function(holiday) {
        let evt = getDefaultEvent();
        evt.ownerId = 1;
        evt.isAllDay = true;
        evt.title = holiday.name;
        evt.start = moment(holiday.date).toDate();
        evt.end = moment(holiday.date).toDate();

        if (holiday.repeatableEveryYear) {
            evt.recurrenceRule = "FREQ=Yearly";
        }
        return evt;
    });
}

module.exports = parser;