/**
 * Created by Khalid Ardah on 1/29/2018.
 */

const CONSTANTS = require("constants");
const getDefaultEvent = require("./defaultEventObject");
const moment = require("moment");

/**
 * Parses json of request type into data source json for schedular
 * @param requests array of requests
 */
function parser(requests) {
    if(!requests) {
        return [];
    }

    // Array of requests (time offs)
    return requests.map(function(request) {
        let evt = getDefaultEvent();
        evt.start = moment(request.startDate).toDate();
        // End date is included so we add 1 day
        evt.end = moment(request.endDate).add(1, "day").toDate();
        evt.title = request.userName;
        evt.description = request.reason;

        if (request.status === CONSTANTS.REQUESTS.APPROVED) {
            evt.title = evt.title + " is off";
            evt.ownerId = 20;
        }else if(request.status === CONSTANTS.REQUESTS.REQUESTED) {
            evt.title = evt.title + " requested time off";
            evt.ownerId = 21;
        }

        return evt;
    });
}

module.exports = parser;