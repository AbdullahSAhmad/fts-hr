/**
 * Created by Khalid Ardah on 1/29/2018.
 */

const getDefaultEvent = require("./defaultEventObject");
const moment = require("moment");

/**
 * Parses json of birthday type into data source json for schedular
 * @param birthdays array of birthdays
 */
function parser(birthdays) {
    if(!birthdays) {
        return [];
    }

    // Array of birthdays
    return birthdays.map(function(birthday) {
        let evt = getDefaultEvent();
        evt.start = moment(birthday.birthday).toDate();
        evt.end = moment(birthday.birthday).toDate();
        evt.title = birthday.userName + "'s Birthday";
        evt.ownerId = 3;
        evt.isAllDay = true;
        evt.recurrenceRule = "FREQ=Yearly";
        return evt;
    });
}

module.exports = parser;