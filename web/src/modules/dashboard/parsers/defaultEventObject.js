/**
 * Created by Khalid Ardah on 1/29/2018.
 */

function getDefaultEvent() {
    return {
        title: "",
        start: new Date(),
        end: new Date(),
        description: "",

        // ownerId: 1 => holidays, 20 => approved time off, 21 => required time off, 3 => birthdays
        ownerId: 0
    };
}

module.exports = getDefaultEvent;