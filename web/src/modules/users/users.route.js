/**
 * Created by Khalid Ardah on 1/8/2018.
 */

const CONSTANTS = require("constants");

const usersRoutes = function(stateProvider) {

    const listState = {
        url: "/users/list",
        templateUrl: "src/modules/users/list/usersList.html",
        controller: "UsersListController"
    };

    const registerState = {
        url: "/users/register",
        templateUrl: "src/modules/users/forms/userForm/userForm.html",
        controller: "UserFormController",
        resolve: {
            user: function() {
                return undefined;
            }
        },
        // Close side nav when enter
        onEnter: onEnter,

        // open side nav when exit
        onExit: onExit
    };

    const editState = {
        url: "/users/edit/:id",
        templateUrl: "src/modules/users/forms/userForm/userForm.html",
        controller: "UserFormController",
        resolve: {
            user: getUser
        },
        // Close side nav when enter
        onEnter: onEnter,

        // open side nav when exit
        onExit: onExit
    };

    const changePasswordState = {
        url: "/users/changePassword/:userId",
        templateUrl: "src/modules/users/forms/changePassword/changePasswordForm.html",
        controller: "ChangePasswordFormController",
        // Close side nav when enter
        onEnter: onEnter,

        // open side nav when exit
        onExit: onExit

    };

    stateProvider.state("layout.users.list", listState);
    stateProvider.state("layout.users.edit", editState);
    stateProvider.state("layout.users.register", registerState);
    stateProvider.state("layout.users.changePassword", changePasswordState);

};

getUser.$inject = ["ftsHttpService", "$stateParams", "$state", "$timeout", "userService"];
function getUser(ftsHttpService, $stateParams, $state, $timeout, userService) {
    // if id is empty go to users list
    if (!$stateParams.id) {
        $timeout(function() {$state.go("layout.users.list");});
        return;
    }

    let url = CONSTANTS.API.USERS.USER + $stateParams.id;

    if (userService.getUser().userId == $stateParams.id) {
        url = CONSTANTS.API.USERS.CURRENT;
    }

    return ftsHttpService.get({url: url})
        .then (function (response) {
            return response.data;
        });
}

onEnter.$inject = ["sideNavService"];
function onEnter(sideNavService){
    sideNavService.openSideNav(false);
}

onExit.$inject = ["sideNavService"];
function onExit(sideNavService) {
    sideNavService.openSideNav(true);
}

module.exports = usersRoutes;