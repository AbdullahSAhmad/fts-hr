/**
 * Created by Khalid Ardah on 1/14/2018.
 */

function usersListOptions(ftsHttpService, moment) {
    return {
        dataSource: require("./dataSourceOptions")(ftsHttpService, moment),
        height: 550,
        groupable: false,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [{
            field: "fullName",
            title: "Basic Information",
            template: `
            <div layout="row" class="basic-info">
                <div flex="20">
                    <img src="assets/img/person.jpg">
                </div>
                <div flex>
                    <h4 class="full-name">{{dataItem.fullName}}</h4>
                    <div class="md-subhead">{{dataItem.email}}</div>
                </div>
            </div>`,
            width: 340
        }, {
            field: "hiringDate",
            title: "Date of Joining",
            template: "{{getFormatedDate(dataItem.hiringDate)}}"
        }, {
            field: "type",
            title: "Role(s)",
            filterable: {
                ui: userTypeFilter,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            }
        }, {
            field: "active",
            title: "Active",
            template: "{{dataItem.active? 'Active': 'Not Active'}}",
            width: 150,
            filterable: {
                ui: userActiveFilter,
                messages: {
                    isTrue: "Active",
                    isFalse: "NOT Active"
                }
            }
        }],
        filterable: {
            extra: false,
            operators: {
                string: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    contains: "Contains",
                    doesnotcontain: "Doesn't contain"
                },
                date: {
                    eq: "Equal",
                    neq: "Not equal",
                    gt: "After",
                    lt: "Before"
                }
            }
        }
    };

    function userTypeFilter(element) {
        element.kendoDropDownList({
            dataSource: ["Admin", "Manager", "HR", "Employee"],
            optionLabel: "--Select Role--"
        });
    }

    function userActiveFilter(element) {
        element.kendoDropDownList({
            dataSource: ["Active", "Not Active"],
            optionLabel: "--Select Status--"
        });
    }
}
module.exports = usersListOptions;
