/**
 * Created by Khalid Ardah on 1/8/2018.
 */


/**
 * users List controller
 */

const usersGridOptions = require("./userListGridOptions");

usersListController.$inject = ["$scope", "moment", "$state", "CONSTANTS", "ftsHttpService"];
function usersListController($scope, moment, $state, CONSTANTS, ftsHttpService) {

    // Init view
    activate();

    function activate() {
        $scope.getFormatedDate = function(date) {
            return moment(date).format(CONSTANTS.FORMAT.DATE);
        };

        // Options for grid
        const options = usersGridOptions(ftsHttpService, moment);
        $scope.kendoComponents = {
            usersGrid: {
                options: options
            }
        };
        initDblclickHandler();
    }

    // Add double click functionality to the rows
    function initDblclickHandler() {
        $scope.$on("kendoWidgetCreated", function(event, widget){
            // Check if the widget is holidaysGrid
            if (widget === $scope.kendoComponents.usersGrid.reference) {
                widget.tbody.on("dblclick","tr[data-uid]", function () {
                    $state.go("layout.users.edit", {id: widget.dataItem(this).id});
                });
            }
        });
    }
}

module.exports = usersListController;