/**
 * Created by Khalid Ardah on 1/14/2018.
 */
const CONSTANTS = require("constants");
function dataSource(ftsHttpService, moment) {
    const options = new kendo.data.DataSource(
        {
            pageSize: 20,
            transport: {
                read: function(options) {
                    ftsHttpService.get({url: CONSTANTS.API.USERS.LIST}).then(
                        function(result) {options.success(result.data);},
                        function(result) {options.error(result);}
                    );
                }
            },
            schema: {
                model: {
                    fields: {
                        birthday: { type: "date" },
                        hiringDate: { type: "date" },
                        email: { type: "string" },
                        fullName: { type: "string" },
                        type: { type: "string" },
                        active: { type: "boolean" }
                    }
                },
                parse: function(data) {
                    // Convert birthday and hiring date to date object
                    $.each(data, function(index, user) {
                        user.birthday = moment(user.birthday).toDate();
                        user.hiringDate = moment(user.hiringDate).toDate();
                    });
                    return data;
                }
            }
        }
    );
    return options;
}

module.exports = dataSource;
