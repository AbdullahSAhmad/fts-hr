/**
 * Created by Khalid Ardah on 1/8/2018.
 */

/**
 * Users module with its controllers
 */

const usersController = require("./users.controller");
const UserFormController = require("./forms/userForm/userForm.controller");
const usersListController = require("./list/usersList.controller");
const ChangePasswordFormController = require("./forms/changePassword/changePasswordForm.controller");
const userFormService = require("./forms/userForm/userFormService");
const changePasswordFormService = require("./forms/changePassword/changePasswordFormService");

/**
 * Users Routes
 */
const usersRoutes = require("./users.route");

const usersModule = angular.module("usersModule", [])
    .controller("UsersController", usersController)
    .controller("UsersListController", usersListController)
    .controller("UserFormController", UserFormController)
    .controller("ChangePasswordFormController", ChangePasswordFormController)

    // users services
    .factory("userFormService", userFormService)
    .factory("changePasswordFormService", changePasswordFormService);

/**
 * Users authenticated routes
 */
usersModule.config(["$stateProvider",
    function ($stateProvider) {
        usersRoutes($stateProvider);
    }
]);
