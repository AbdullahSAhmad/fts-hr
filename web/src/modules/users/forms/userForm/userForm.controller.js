/**
 * Created by Khalid Ardah on 1/8/2018.
 */


/**
 * user Form controller
 * @param userFormService
 */

userFormController.$inject = ["$scope", "userFormService", "moment", "$state", "user", "userService", "CONSTANTS"];
function userFormController($scope, userFormService, moment, $state, user, userService, CONSTANTS) {

    // Init form
    activate();

    // Init handler
    $scope.handlers = {
        submit: submit,
        activateUser: activateUser,
        deleteUser: deleteUser,
        close: close
    };

    function activate() {
        // Pass user to the scope
        $scope.user = userService.getUser();

        // Maximum birthday date
        $scope.maxBirthday = moment().subtract(20, "years").toDate();

        // Minimum birthday date
        $scope.minBirthday = moment().subtract(70, "years").toDate();

        // Current date for max hiring date
        $scope.currentDate = moment().toDate();

        $scope.maxDaysOff = 14;

        // User default model for the form
        $scope.model =  {
            birthday: $scope.maxBirthday,
            email: "",
            fullName: "",
            hiringDate: moment().toDate(),
            password: "",
            personalId: "",
            remainingDaysOff: 14,
            type: "",
            userName: "",
            active: true
        };

        // If in edit route
        if (user) {
            $scope.isEditMode = true;
            $scope.model = user;
            $scope.maxDaysOff = 20;
            $scope.model.birthday = moment(user.birthday).toDate();
            $scope.model.hiringDate = moment(user.hiringDate).toDate();
        }

        // $scope.user refers to the logged in user
        // user refers to the user to be edited
        // Add admin option if im editing an admin but disable it
        const showAdminOption = user && user.type === "Admin";

        // Add the 2 options below for hr if im editing but disable it
        const showHrOption = user || ($scope.user.meta && $scope.user.meta.type!="HR");
        const showManagerOption = user || ($scope.user.meta && $scope.user.meta.type!="HR");

        // Disable it if im not admin and im editing
        const disableUserTypes = user && ($scope.user.meta && $scope.user.meta.type!="Admin");

        $scope.flags = {
            showAdminOption: showAdminOption,
            showHrOption: showHrOption,
            showManagerOption: showManagerOption,
            disableUserTypes: disableUserTypes,
            showActionBts: $scope.isEditMode && !userService.checkUserType(CONSTANTS.USER.EMPLOYEE) && (user.id != $scope.user.userId)
        };
    }

    function submit(user) {
        if ($scope.isEditMode) {
            userFormService.editUser(user).then(function success(data) {
                redirectToEdit(data.id);
            }, handleError);
        }
        else {
            userFormService.registerUser(user).then(function success(data) {
                redirectToEdit(data.id);
            }, handleError);
        }
    }

    // Activate/Deactivate user
    function activateUser(id, val) {
        userFormService.activateUser(id, val).then(function success() {
            $scope.model.active = val;
            user.active = val;
        }, handleError);
    }

    // Deletes the user
    function deleteUser(id) {
        userFormService.deleteUser(id).then(function success() {
            redirectToList();
        }, handleError);
    }

    function close() {
        redirectToList();
    }

    function redirectToList() {
        $state.go("layout.users.list");
    }

    function redirectToEdit(id) {
        $state.go("layout.users.edit", {id: id});
    }

    function handleError(error) {
        $scope.focused = false;
        $scope.formErrorMessage = error.data.responseStatus.message;
    }
}

module.exports = userFormController;