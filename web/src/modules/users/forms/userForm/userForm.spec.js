require("../../../../../node_modules/ngstorage/ngStorage");
require("../../../../../node_modules/angular-ui-router");
const moment = require("moment");
const CONSTANTS = require("../../../../shared/constants");

describe("RequestFormController", function() {
    let $controller, controller, scope, $rootScope, $httpBackend, state, userService;

    beforeEach(function() {
        angular.mock.module("ui.router");
        angular.mock.module("coreModule");
        angular.mock.module(function ($provide) {
            $provide.value("CONSTANTS", CONSTANTS);
            $provide.value("moment", moment);
        });
    });
    beforeEach(angular.mock.module("usersModule"));

    beforeEach(inject(function(_$controller_, _$rootScope_, _$httpBackend_, _$q_, _$state_, _userService_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = _$state_;
        userService = _userService_;

        // spy on function request to supply fake user
        spyOn(userService, "getUser").and.callFake(function() {
            return {
                userId: 1,
                userName: "test"
            };
        });

        scope = $rootScope.$new();
        controller = $controller("UserFormController", { $scope: scope, user: undefined });
    }));

    it("Controller should be created", function() {
        expect(controller).toBeDefined();
    });

    it("add valid user", function() {

        // fill data
        scope.model = getUser();

        // spy on function request and expect to route to edit state
        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.users.edit");
        });

        // submit the values
        scope.handlers.submit(scope.model);

        // dummy response
        $httpBackend
            .when("POST", CONSTANTS.API.USERS.REGISTER)
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();

        // expects add mode
        expect(scope.isEditMode).toBeUndefined();
        // added successfully then moved to edit state
        expect(state.go).toHaveBeenCalled();
    });

    it("add invalid user", function() {

        // fill data
        scope.model = getUser();

        // spy on function request and expect to route to edit state
        spyOn(state, "go");

        // submit the values
        scope.handlers.submit(scope.model);

        const message = "invalid period";
        // dummy response
        $httpBackend
            .when("POST", CONSTANTS.API.USERS.REGISTER)
            .respond(400, {
                status: "bad request",
                responseStatus: {message: message}
            });
        $httpBackend.flush();

        // should display a message
        expect(scope.focused).toBeFalsy();
        expect(scope.formErrorMessage).toEqual(message);

        // check if it is edit mode
        expect(scope.isEditMode).toBeUndefined();

        // state.go shouldn't be called
        expect(state.go).not.toHaveBeenCalled();
    });

    it("edit user with success status", function() {

        let $scope = $rootScope.$new();
        $controller("UserFormController", { $scope: $scope, user: getUser() });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.users.edit");
        });

        $scope.handlers.submit($scope.model);

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.USERS.USER + $scope.model.id)
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("edit user with failure", function() {

        let $scope = $rootScope.$new();
        $controller("UserFormController", { $scope: $scope, user: getUser() });

        $scope.handlers.submit($scope.model);
        const message = "invalid date";

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.USERS.USER + $scope.model.id)
            .respond(400, {
                status: "bad request",
                responseStatus: {message: message}
            });
        $httpBackend.flush();

        // should display a message
        expect($scope.focused).toBeFalsy();
        expect($scope.formErrorMessage).toEqual(message);

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("remove user", function() {
        let $scope = $rootScope.$new();
        const user = getUser();
        $controller("UserFormController", { $scope: $scope, user: user });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // deleted successfully
            expect(stateParam).toEqual("layout.users.list");
        });

        $scope.handlers.deleteUser(user.id);

        // dummy response
        $httpBackend
            .when("DELETE", CONSTANTS.API.USERS.USER + user.id)
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();
    });
});

function getUser() {
    const user = {
        id: 2,
        userName: "testing",
        personalId: "12345867891",
        email: "test1@test1.com",
        birthday: "1995-12-19",
        hiringDate: "2019-01-01",
        remainingDaysOff: 14,
        remainingSickDaysOff: 14,
        active: true,
        type: "Employee",
        fullName: "test user"
    };

    return user;
}