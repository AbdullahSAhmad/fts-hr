/**
 * Created by Khalid Ardah on 1/9/2018.
 */


/**
 * User registration service
 * Service name: userRegFormService
 * @param ftsHttpService
 */

userRegFormService.$inject = ["ftsHttpService", "CONSTANTS"];

function userRegFormService(ftsHttpService, CONSTANTS) {

    return {
        registerUser: registerUser,
        editUser: editUser,
        activateUser: activateUser,
        deleteUser: deleteUser
    };

    function registerUser(user) {
        return ftsHttpService.post({url: CONSTANTS.API.USERS.REGISTER, data: user});
    }

    function editUser(user) {
        return ftsHttpService.put({url: CONSTANTS.API.USERS.USER + user.id, data: user});
    }

    function activateUser(userId, val) {
        if (val) {
            return ftsHttpService.post({url: CONSTANTS.API.USERS.ACTIVE + userId});
        }
        else {
            return ftsHttpService.delete({url: CONSTANTS.API.USERS.ACTIVE + userId});
        }
    }

    function deleteUser(userId) {
        return ftsHttpService.delete({url: CONSTANTS.API.USERS.USER + userId});
    }
}

module.exports = userRegFormService;