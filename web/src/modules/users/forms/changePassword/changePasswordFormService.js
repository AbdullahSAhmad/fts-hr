/**
 * Created by Khalid Ardah on 1/16/2018.
 */


/**
 * User change password service
 * Service name: changePasswordFormService
 * @param ftsHttpService
 */

changePasswordFormService.$inject = ["ftsHttpService", "CONSTANTS"];

function changePasswordFormService(ftsHttpService, CONSTANTS) {

    return {
        resetPassword: resetPassword
    };

    function resetPassword(userId, password) {
        return ftsHttpService.put({url: CONSTANTS.API.USERS.RESET_PASSWORD + userId, data: {newPassword: password}});
    }
}

module.exports = changePasswordFormService;