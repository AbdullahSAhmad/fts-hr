/**
 * Created by Khalid Ardah on 1/16/2018.
 */


/**
 * change Password Form controller
 */

changePasswordFormController.$inject = ["$scope", "$state","$stateParams", "changePasswordFormService"];
function changePasswordFormController($scope, $state, $stateParams, changePasswordFormService) {

    // Init form
    activate();

    // Init handlers
    $scope.handlers = {
        submit: submit,
        cancel: cancel
    };

    function activate() {
        $scope.model = {
            newPassword: "",
            passwordConfirm: ""
        };
    }

    function submit(formModel) {
        if (formModel.newPassword !== formModel.passwordConfirm) {
            $scope.passwordChangeForm.passwordConfirm.$setValidity("passwordVerify", false);
        }
        else {
            // API req
            changePasswordFormService.resetPassword($stateParams.userId, formModel.newPassword).then(
                function success() {redirectToEdit($stateParams.userId);}
            );
        }
    }

    function cancel() {
        redirectToEdit($stateParams.userId);
    }

    function redirectToEdit(id) {
        $state.go("layout.users.edit", {id: id});
    }
}

module.exports = changePasswordFormController;