/**
 * Created by Khalid Ardah on 1/16/2018.
 */

/**
 * Requests module with its controllers
 */

const RequestsController = require("./requests.controller");
const RequestFormController = require("./forms/requestForm/requestForm.controller");
const RequestsListController = require("./list/requestsList.controller");
const requestFormService = require("./forms/requestForm/requestFormService");
const requestActionDirective = require("./forms/requestForm/requestAction/requestActionsDirective");

/**
 * requests Routes
 */
const requestsRoutes = require("./requests.route");

const requestsModule = angular.module("requestsModule", [])
    .controller("RequestsController", RequestsController)
    .controller("RequestsListController", RequestsListController)
    .controller("RequestFormController", RequestFormController)

    // requests services
    .factory("requestFormService", requestFormService)

    .directive("requestAction", requestActionDirective);

/**
 * Requests authenticated routes
 */
requestsModule.config(
    function ($stateProvider) {
        requestsRoutes($stateProvider);
    }
);
