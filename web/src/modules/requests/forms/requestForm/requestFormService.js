/**
 * Created by Khalid Ardah on 1/16/2018.
 */


/**
 * User time off service
 * Service name: requestFormService
 * @param ftsHttpService
 */

requestFormService.$inject = ["ftsHttpService", "CONSTANTS"];

function requestFormService(ftsHttpService, CONSTANTS) {

    return {
        getReviewersList: getReviewersList,
        requestTimeOff: requestTimeOff,
        editTimeOff: editTimeOff,
        deleteTimeOff: deleteTimeOff,
        approveTimeOff: approveTimeOff,
        denyTimeOff: denyTimeOff,
        reapplyTimeOff: reapplyTimeOff,
        reassignTimeOff: reassignTimeOff
    };

    function getReviewersList() {
        return ftsHttpService.get({url: CONSTANTS.API.REQUESTS.REVIEWERS});
    }

    function approveTimeOff(id) {
        return ftsHttpService.put({url: CONSTANTS.API.REQUESTS.APPROVE + id});
    }

    function denyTimeOff(id, message) {
        return ftsHttpService.put({url: CONSTANTS.API.REQUESTS.DENY + id, data: {message: message}});
    }

    function reapplyTimeOff(id, message) {
        return ftsHttpService.put({url: CONSTANTS.API.REQUESTS.REAPPLY + id, data: {message: message}});
    }

    function reassignTimeOff(id, reviewerId) {
        return ftsHttpService.put({url: CONSTANTS.API.REQUESTS.ASSIGN + id, data: {reviewerId: reviewerId}});
    }

    function requestTimeOff(request) {
        return ftsHttpService.post({url: CONSTANTS.API.REQUESTS.APPLY, data: request});
    }

    function editTimeOff(request) {
        return ftsHttpService.put({url: CONSTANTS.API.REQUESTS.EDIT + request.id, data: request});
    }

    function deleteTimeOff(id) {
        return ftsHttpService.delete({url: CONSTANTS.API.REQUESTS.REQUEST + id});
    }
}

module.exports = requestFormService;