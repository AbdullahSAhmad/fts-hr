/**
 * Created by Khalid Ardah on 1/8/2018.
 */


/**
 * request Form controller
 * @param requestFormService
 */

requestFormController.$inject = ["$scope", "requestFormService", "moment", "$state", "request", "userService", "CONSTANTS"];
function requestFormController($scope, requestFormService, moment, $state, request, userService, CONSTANTS) {

    // Handler functions
    $scope.handlers = {
        submit: submit,
        applyAction: applyAction,
        approveRequest: requestFormService.approveTimeOff,
        denyRequest: requestFormService.denyTimeOff,
        reapplyRequest: requestFormService.reapplyTimeOff,
        reassignRequest: requestFormService.reassignTimeOff,
        deleteRequest: requestFormService.deleteTimeOff,
        close: close
    };

    activate();

    function activate() {
        $scope.user = userService.getUser();
        // Current date for max time off date
        $scope.currentDate = moment().toDate();

        // Request default model for the form
        $scope.model = {
            startDate: moment().toDate(),
            endDate: moment().add(1, "d").toDate(),
            type: "",
            reason: "",
            userId: $scope.user.userId
        };

        $scope.flags = {
            showApproveBtn: false,
            showDenyBtn: false,
            showDeleteBtn: false,
            showReapplyBtn: false,
            disableForm: false,
            showDenyDialog: false,
            showReapplyDialog: false,
            showReassignBtn: false
        };

        // If in edit route
        if (request) {
            $scope.isEditMode = true;
            $scope.model = request;
            $scope.model.startDate = moment(request.startDate).toDate();
            $scope.model.endDate = moment(request.endDate).toDate();

            const requestedStatus =  request.status==CONSTANTS.REQUESTS.REQUESTED;
            const deniedStatus = request.status===CONSTANTS.REQUESTS.DENIED;

            const correctReviewer = request.reviewerId==$scope.user.userId;
            const sameUser = request.userId==$scope.user.userId;

            $scope.flags.showApproveBtn = requestedStatus && correctReviewer;
            $scope.flags.showDenyBtn = requestedStatus && correctReviewer;

            $scope.flags.showDeleteBtn = requestedStatus && (correctReviewer || sameUser);
            $scope.flags.showReapplyBtn = deniedStatus && request.userId==$scope.user.userId;

            $scope.flags.showReassignBtn = requestedStatus && !userService.checkUserType(CONSTANTS.USER.EMPLOYEE);

            if ($scope.flags.showReassignBtn) {
                $scope.reviewersList = [];
                requestFormService.getReviewersList().then(function success(response) {
                    $scope.reviewersList = response.data;
                });
            }

            // Disable the form if im not request owner
            $scope.flags.disableForm = request.userId!=$scope.user.userId;
        }

        $scope.getFormattedDate = function(date) {
            return moment(date).format(CONSTANTS.FORMAT.DATE);
        };
    }

    function submit(request) {
        if ($scope.isEditMode) {
            requestFormService.editTimeOff(request).then(function success() {
                redirectToEdit(request.id);
            }, function error(error) {
                handleRequestError(error);
            });
        }
        else {
            requestFormService.requestTimeOff(request).then(function success(data) {
                redirectToEdit(data.id);
            }, function error(error) {
                handleRequestError(error);
            });
        }
    }

    function applyAction(action, id, actionModel) {
        return action(id, actionModel).then(function success() {
            redirectToList();
        }, function error(error) {
            handleRequestError(error);
        });
    }

    function close() {
        redirectToList();
    }

    function redirectToList() {
        $state.go("layout.requests.list");
    }

    function redirectToEdit(id) {
        $state.go("layout.requests.edit", {id: id});
    }

    function handleRequestError(error) {
        $scope.focused = false;
        $scope.formErrorMessage = error.data.responseStatus? error.data.responseStatus.message : "Unknown Error";
    }
}

module.exports = requestFormController;