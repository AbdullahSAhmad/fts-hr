/**
 * Created by Khalid Ardah on 1/21/2018.
 */

/**
 * Directive name: request-action
 */
module.exports = [
    function() {
        return {
            scope: {
                action: "&",
                showDialog: "=",
                reviewersList: "=",
                actionModel: "="
            },
            restrict: "E",
            templateUrl: "src/modules/requests/forms/requestForm/requestAction/requestActionsDirective.html",
            link: function($scope) {
                // Handlers
                $scope.handlers = {
                    submit: submit,
                    close: close
                };


                function submit(actionModel) {
                    $scope.loading = true;
                    $scope.action({actionModel: actionModel}).finally(function() {
                        $scope.loading = false;
                    });
                }

                function close() {
                    $scope.showDialog = false;
                }
            }
        };
    }
];