require("../../../../../node_modules/ngstorage/ngStorage");
require("../../../../../node_modules/angular-ui-router");
const moment = require("moment");
const CONSTANTS = require("../../../../shared/constants");

describe("RequestFormController", function() {
    let $controller, controller, scope, $rootScope, $httpBackend, state, userService;

    beforeEach(function() {
        angular.mock.module("ui.router");
        angular.mock.module("coreModule");
        angular.mock.module(function ($provide) {
            $provide.value("CONSTANTS", CONSTANTS);
            $provide.value("moment", moment);
        });
    });
    beforeEach(angular.mock.module("requestsModule"));


    beforeEach(inject(function(_$controller_, _$rootScope_, _$httpBackend_, _$q_, _$state_, _userService_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = _$state_;
        userService = _userService_;

        // spy on function request to supply fake user
        spyOn(userService, "getUser").and.callFake(function() {
            return {
                userId: 1,
                userName: "test"
            };
        });

        scope = $rootScope.$new();
        controller = $controller("RequestFormController", { $scope: scope, request: undefined });
    }));

    it("Controller should be created", function() {
        expect(controller).toBeDefined();
    });

    it("request valid time off", function() {

        // fill data
        const model = getRequest();

        scope.model = model;

        // expects add mode
        expect(scope.isEditMode).toBeUndefined();

        // spy on function request and expect to route to edit state
        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.requests.edit");
        });

        // submit the values
        scope.handlers.submit(scope.model);

        // dummy response
        $httpBackend
            .when("POST", CONSTANTS.API.REQUESTS.APPLY)
            .respond(200, {
                status: "success"
            });
        $httpBackend.flush();

        // added successfully then moved to edit state
        expect(state.go).toHaveBeenCalled();
    });

    it("request invalid time off", function() {

        // fill data
        const model = getRequest();
        model.endDate = moment(model.startDate).subtract(1, "day");

        scope.model = model;

        // submit the values
        scope.handlers.submit(scope.model);

        const message = "invalid period";
        // dummy response
        $httpBackend
            .when("POST", CONSTANTS.API.REQUESTS.APPLY)
            .respond(400, {
                status: "bad request",
                responseStatus: {message: message}
            });
        $httpBackend.flush();

        // should display a message
        expect(scope.focused).toBeFalsy();
        expect(scope.formErrorMessage).toEqual(message);

        // check if it is edit mode
        expect(scope.isEditMode).toBeUndefined();
    });

    it("edit request with success status", function() {

        let $scope = $rootScope.$new();
        let request = getRequest();
        request.status = "Requested";

        $controller("RequestFormController", { $scope: $scope, request: request });

        spyOn(state, "go").and.callFake(function(stateParam, params) {
            // done successfully
            expect(stateParam).toEqual("layout.requests.edit");
            expect(params.id.toString()).toEqual("1");
        });

        $scope.handlers.submit($scope.model);

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.REQUESTS.EDIT + $scope.model.id)
            .respond(200, {
                status: "success"
            });

        // dummy response returns array of reviewers
        $httpBackend
            .when("GET", CONSTANTS.API.REQUESTS.REVIEWERS)
            .respond(200, []);
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("edit request with failure", function() {

        let $scope = $rootScope.$new();
        let request = getRequest();
        request.status = "Requested";

        $controller("RequestFormController", { $scope: $scope, request: request });

        $scope.handlers.submit($scope.model);

        const message = "invalid period";

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.REQUESTS.EDIT + $scope.model.id)
            .respond(400, {
                responseStatus: {message: message}
            });

        // dummy response returns array of reviewers
        $httpBackend
            .when("GET", CONSTANTS.API.REQUESTS.REVIEWERS)
            .respond(200, []);
        $httpBackend.flush();

        // should display a message
        expect($scope.focused).toBeFalsy();
        expect($scope.formErrorMessage).toEqual(message);

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("deny request", function() {

        let $scope = $rootScope.$new();
        let request = getRequest();
        request.status = "Requested";

        $controller("RequestFormController", { $scope: $scope, request: request });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.requests.list");
        });

        $scope.handlers.denyRequest($scope.model.id, "im testing deny");

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.REQUESTS.DENY + $scope.model.id)
            .respond(200, {
                status: "success"
            });

        // dummy response returns array of reviewers
        $httpBackend
            .when("GET", CONSTANTS.API.REQUESTS.REVIEWERS)
            .respond(200, []);
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("approve request", function() {

        let $scope = $rootScope.$new();
        let request = getRequest();
        request.status = "Requested";

        $controller("RequestFormController", { $scope: $scope, request: request });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.requests.list");
        });

        $scope.handlers.approveRequest($scope.model.id);

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.REQUESTS.APPROVE + $scope.model.id)
            .respond(200, {
                status: "success"
            });

        // dummy response returns array of reviewers
        $httpBackend
            .when("GET", CONSTANTS.API.REQUESTS.REVIEWERS)
            .respond(200, []);
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("reassign request", function() {

        let $scope = $rootScope.$new();
        let request = getRequest();
        request.status = "Requested";

        $controller("RequestFormController", { $scope: $scope, request: request });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.requests.list");
        });

        $scope.handlers.reassignRequest($scope.model.id, 1);

        // dummy response
        $httpBackend
            .when("PUT", CONSTANTS.API.REQUESTS.ASSIGN + $scope.model.id)
            .respond(200, {
                status: "success"
            });

        // dummy response returns array of reviewers
        $httpBackend
            .when("GET", CONSTANTS.API.REQUESTS.REVIEWERS)
            .respond(200, [
                {id: 1,userName: "test approver"}
            ]);
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    it("remove request", function() {

        let $scope = $rootScope.$new();
        let request = getRequest();
        request.status = "Requested";

        $controller("RequestFormController", { $scope: $scope, request: request });

        spyOn(state, "go").and.callFake(function(stateParam) {
            // done successfully
            expect(stateParam).toEqual("layout.requests.list");
        });

        $scope.handlers.deleteRequest($scope.model.id);

        // dummy response
        $httpBackend
            .when("DELETE", CONSTANTS.API.REQUESTS.DELETE + $scope.model.id)
            .respond(200, {
                status: "success"
            });

        // dummy response returns array of reviewers
        $httpBackend
            .when("GET", CONSTANTS.API.REQUESTS.REVIEWERS)
            .respond(200, [
                {id: 1,userName: "test approver"}
            ]);
        $httpBackend.flush();

        // check if it is edit mode
        expect($scope.isEditMode).toBeTruthy();
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
});

function getRequest() {
    const startDate = moment().add(1, "month").toDate();
    const endDate = moment().add(1, "month").add(2, "day").toDate();

    const request = {
        startDate: startDate,
        endDate: endDate,
        type: "Paid",
        reason: "i will travel",
        userId: 1,
        status: "Requested",
        id: 1
    };

    return request;
}