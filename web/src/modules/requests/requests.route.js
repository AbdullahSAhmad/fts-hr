/**
 * Created by Khalid Ardah on 1/16/2018.
 */

const CONSTANTS = require("constants");

const requestsRoutes = function(stateProvider) {

    const listState = {
        url: "/requests/list",
        templateUrl: "src/modules/requests/list/requestsList.html",
        controller: "RequestsListController"
    };

    const editState = {
        url: "/requests/edit/:id",
        templateUrl: "src/modules/requests/forms/requestForm/requestForm.html",
        controller: "RequestFormController",
        resolve: {
            request: getRequest
        },
        // Close side nav when enter
        onEnter: onEnter,
        // open side nav when exit
        onExit: onExit
    };

    const applyState = {
        url: "/requests/apply",
        templateUrl: "src/modules/requests/forms/requestForm/requestForm.html",
        controller: "RequestFormController",
        resolve: {
            request: function() {
                return undefined;
            },
        },
        // Close side nav when enter
        onEnter: onEnter,
        // open side nav when exit
        onExit: onExit
    };

    stateProvider.state("layout.requests.list", listState);
    stateProvider.state("layout.requests.edit", editState);
    stateProvider.state("layout.requests.apply", applyState);

};

/**
 * Gets the request for request edit state
 */
getRequest.$inject = ["ftsHttpService", "$stateParams", "$state", "$timeout"];
function getRequest(ftsHttpService, $stateParams, $state, $timeout) {
    // if id is empty go to requests list
    if (!$stateParams.id) {
        $timeout(function() {$state.go("layout.requests.list");});
        return;
    }

    return ftsHttpService.get({url: CONSTANTS.API.REQUESTS.REQUEST + $stateParams.id})
        .then (
            function success(response) {
                return response.data;
            },
            function error() {
                $timeout(function() {$state.go("layout.requests.list");});
            });
}

onEnter.$inject = ["sideNavService"];
function onEnter(sideNavService){
    sideNavService.openSideNav(false);
}

onExit.$inject = ["sideNavService"];
function onExit(sideNavService){
    sideNavService.openSideNav(true);
}

module.exports = requestsRoutes;