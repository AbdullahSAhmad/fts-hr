/**
 * Created by Khalid Ardah on 1/16/2018.
 */

function usersListOptions(ftsHttpService, userId, moment) {
    return {
        dataSource: require("./dataSourceOptions")(ftsHttpService, userId, moment),
        height: 550,
        groupable: false,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [{
            field: "userName",
            title: "Name",
            width: 240
        }, {
            field: "startDate",
            title: "Start Date",
            template: "{{getFormatedDate(dataItem.startDate)}}",
            width: 340
        }, {
            field: "endDate",
            title: "End Date",
            template: "{{getFormatedDate(dataItem.endDate)}}"
        }, {
            field: "type",
            title: "Type",
            filterable: {
                ui: requestTypeFilter,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            }
        }, {
            field: "status",
            title: "Status",
            filterable: {
                ui: requestStatusFilter,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            }
        }, {
            field: "reason",
            title: "Reason",
            width: 150
        }],
        filterable: {
            extra: false,
            operators: {
                string: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    contains: "Contains",
                    doesnotcontain: "Doesn't contain"
                },
                date: {
                    eq: "Equal",
                    neq: "Not equal",
                    gt: "After",
                    lt: "Before"
                }
            }
        }
    };

    function requestStatusFilter(element) {
        element.kendoDropDownList({
            dataSource: ["Requested", "Approved", "Denied"],
            optionLabel: "--Select Status--"
        });
    }

    function requestTypeFilter(element) {
        element.kendoDropDownList({
            dataSource: ["Paid", "Sick", "Not Paid"],
            optionLabel: "--Select Type--"
        });
    }
}
module.exports = usersListOptions;
