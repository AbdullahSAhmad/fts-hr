/**
 * Created by Khalid Ardah on 1/16/2018.
 */


/**
 * requests List controller
 */

const requestsGridOptions = require("./requestsListGridOptions");

requestsListController.$inject = ["$scope", "moment", "$state", "CONSTANTS", "ftsHttpService", "userService"];
function requestsListController($scope, moment, $state, CONSTANTS, ftsHttpService, userService) {

    activate();

    function activate() {
        $scope.getFormatedDate = function(date) {
            return moment(date).format(CONSTANTS.FORMAT.DATE);
        };

        // Options for grid
        const options = requestsGridOptions(ftsHttpService, userService.getUser().userId, moment);
        $scope.kendoComponents = {
            requestsGrid: {
                options: options
            }
        };
        initDblclickHandler();
    }

    // Add double click functionality to the rows
    function initDblclickHandler() {
        $scope.$on("kendoWidgetCreated", function(event, widget){
            // Check if the widget is holidaysGrid
            if (widget === $scope.kendoComponents.requestsGrid.reference) {
                widget.tbody.on("dblclick","tr[data-uid]", function () {
                    $state.go("layout.requests.edit", {id: widget.dataItem(this).id});
                });
            }
        });
    }
}

module.exports = requestsListController;