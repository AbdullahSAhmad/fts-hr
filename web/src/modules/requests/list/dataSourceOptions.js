/**
 * Created by Khalid Ardah on 1/16/2018.
 */
const CONSTANTS = require("constants");
function dataSource(ftsHttpService, userId, moment) {
    const options = new kendo.data.DataSource(
        {
            pageSize: 20,
            transport: {
                read: function(options) {
                    ftsHttpService.get({url: CONSTANTS.API.REQUESTS.USER + userId}).then(
                        function(result) {options.success(result.data);},
                        function(result) {options.error(result);}
                    );
                }
            },
            schema: {
                model: {
                    fields: {
                        startDate: { type: "date" },
                        endDate: { type: "date" },
                        status: { type: "string" },
                        type: { type: "string" },
                        reason: { type: "string" }
                    }
                },
                parse: function(data) {
                    // Convert start date and end date to date object
                    $.each(data, function(index, request) {
                        request.startDate = moment(request.startDate).toDate();
                        request.endDate = moment(request.endDate).toDate();
                    });
                    return data;
                }
            }
        }
    );

    return options;
}

module.exports = dataSource;
