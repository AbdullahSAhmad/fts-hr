/**
 * Created by Khalid Ardah on 12/27/2018.
 */

module.exports = {
    API: {
        LOGIN: "/auth",
        LOGOUT: "/auth/logout",
        USERS: {
            REGISTER: "/api/users",
            USER: "/api/users/",
            LIST: "/api/users",
            RESET_PASSWORD: "/api/users/resetPassword/",
            ACTIVE: "/api/users/active/",
            CURRENT: "/api/users/current"
        },
        REQUESTS: {
            LIST: "/api/timeOffs/notDenied",
            USER: "/api/timeOffs/users/",
            APPLY: "/api/timeOffs",
            REQUEST: "/api/timeOffs/",
            EDIT: "/api/timeOffs/save/",
            APPROVE: "/api/timeOffs/approve/",
            DENY: "/api/timeOffs/deny/",
            REAPPLY: "/api/timeOffs/reapply/",
            REVIEWERS: "/api/users/reviewers",
            ASSIGN: "/api/timeOffs/assign/"
        },
        HOLIDAYS: {
            HOLIDAY: "/api/holidays/",
            LIST: "/api/holidays",
            CREATE: "/api/holidays"
        },
        BIRTHDAYS: {
            LIST: "/api/users/birthdays"
        }
    },
    FORMAT: {
        DATE: "DD-MMM-YYYY"
    },
    USER: {
        EMPLOYEE: "Employee",
        MANAGER: "Manager"
    },
    REQUESTS: {
        REQUESTED: "Requested",
        DENIED: "Denied",
        APPROVED: "Approved"
    }
};