/**
 * Created by Khalid Ardah on 12/27/2018.
 */

/**
 * Global Styles
 */
require("./styles/styles.scss");

/*
* APP Dependencies
* */
require("angular");
require("angular-route");
require("angular-ui-router");
require("angular-animate/angular-animate");
require("angular-aria/angular-aria");
require("angular-material/angular-material");
require("ngstorage/ngStorage");
require("angular-messages/angular-messages");
require("angular-sanitize/angular-sanitize");
require("js/kendo/js/kendo.all.min.js");
require("js/fontawesome/v5.6.3.js");

/**
 * App Imports
*/
require("src/modules/core/core.module.js");
require("src/modules/login/login.module.js");
require("src/modules/requests/requests.module.js");
require("src/modules/dashboard/dashboard.module.js");
require("src/modules/holidays/holidays.module.js");
require("src/modules/users/users.module.js");
require("src/modules/layout/layout.module.js");

require("src/app.module.js");
