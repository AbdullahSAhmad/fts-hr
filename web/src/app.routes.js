/**
 * Created by Khalid Ardah on 12/27/2018.
 */

const appRoutes = function(stateProvider, urlRouterProvider) {

    // App main 2 routes
    const layoutState = {
        abstract: true,
        url: "",
        templateUrl: "src/modules/layout/layout.html",
        controller: "LayoutController"
    };

    const loginState = {
        url: "/login",
        templateUrl: "src/modules/login/login.html",
        controller: "LoginController"
    };

    stateProvider.state("layout", layoutState);
    stateProvider.state("login", loginState);

    urlRouterProvider.otherwise(function($injector){
        var $state = $injector.get("$state");
        $state.go("layout.dashboard");
    });

};

module.exports = appRoutes;