/**
 * Created by Khalid Ardah on 12/27/2018.
 */

"use strict";
const appRoutes = require("./app.routes.js");

const app = angular.module("ftsApp", ["ui.router", "ngMaterial", "ngMessages", "ngSanitize", "coreModule", "loginModule", "layoutModule", "kendo.directives"]).config(
    function ($stateProvider, $urlRouterProvider) {

        // main app routes
        appRoutes($stateProvider, $urlRouterProvider);
    }
);

/*
* Controllers
* */
app.controller("GlobalController", ["$rootScope", "_", "authService", "$state", "userService", "CONSTANTS", function ($rootScope, _, authService, $state, userService, CONSTANTS) {
    //State changes for auth
    $rootScope.$on("$stateChangeStart",
        function(event, toState) {

            // Redirect to dashboard if the user is logged in
            if (authService.isLoggedIn()) {
                if (toState.name === "login") {
                    event.preventDefault();
                    $state.go("layout.dashboard");
                }
                // Redirect if the user is employee and trying to route to users list
                else if (userService.checkUserType(CONSTANTS.USER.EMPLOYEE) &&
                        (toState.name.includes("layout.users.list") || toState.name.includes("layout.dashboard"))) {
                    event.preventDefault();
                    $state.go("layout.requests.list");
                }
            }

            // Redirect to login if the user is logged out
            else if (!authService.isLoggedIn() && _.startsWith(toState.name, "layout")) {
                event.preventDefault();
                $state.go("login");
            }
        });
}]);

// Adding momentjs to the project
app.constant("moment", require("moment"));
// Adding lodash to the project
app.constant("_", require("lodash/lodash"));
// Adding CONSTANTS to the project
app.constant("CONSTANTS", require("./shared/constants"));