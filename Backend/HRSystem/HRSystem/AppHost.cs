﻿using Funq;
using ServiceStack;
using HRSystem.ServiceInterface;
using ServiceStack.OrmLite;
using ServiceStack.Data;
using ServiceStack.Auth;
using ServiceStack.Caching;
using ServiceStack.Text;
using System;
using System.Globalization;
using HRSystem.ServiceModel;
using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceInterface.BusinessLogic;
using ServiceStack.OrmLite.SqlServer.Converters;
using ServiceStack.Validation;
using HRSystem.ServiceInterface.Validator;

namespace HRSystem
{
    //VS.NET Template Info: https://servicestack.net/vs-templates/EmptyAspNet
    public class AppHost : AppHostBase
    {
        /// <summary>
        /// Base constructor requires a Name and Assembly where web service implementation is located
        /// </summary>
        public AppHost()
            : base("HRSystem", typeof(MyServices).Assembly) { }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        public override void Configure(Container container)
        {
            Plugins.Add(new ValidationFeature());
            container.RegisterValidators(typeof(CreateUserValidator).Assembly);
            string connectionString = AppSettings.Get<string>("ConnectionString");
            SqlServerConverters.Configure(SqlServer2017Dialect.Provider);
            var DbConnectionFactory = new OrmLiteConnectionFactory(connectionString,SqlServer2017Dialect.Provider);
            container.Register<IDbConnectionFactory>(DbConnectionFactory);
            var privateKey = RsaUtils.CreatePrivateKeyParams(RsaKeyLengths.Bit2048);
            var publicKey = privateKey.ToPublicRsaParameters();
            var privateKeyXml = privateKey.ToPrivateKeyXml();
            var publicKeyXml = privateKey.ToPublicKeyXml();
            Plugins.Add(new AuthFeature(() => (new AuthUserSession()),
            new IAuthProvider[] {
                new CredentialsAuthProvider(),
                new JwtAuthProvider()
                {
                    HashAlgorithm = "RS256",
                    PrivateKeyXml = privateKeyXml,
                    PublicKeyXml = publicKeyXml,
                    RequireSecureConnection = false
                },
            }));
            container.Register<ICacheClient>(new MemoryCacheClient());
            container.Register<IAuthRepository>(c => new OrmLiteAuthRepository<CustoemUserAuth, UserAuthDetails>(c.Resolve<IDbConnectionFactory>()));
            container.Resolve<IAuthRepository>().InitSchema();
            container.Register<ICustomUserRepostiry>(c => new CustomUserRepostiry
            {
                DbConnectionFactory = c.Resolve<IDbConnectionFactory>(),
                AuthRepository = c.Resolve<IAuthRepository>()
            });
            var authRepo = container.Resolve<IAuthRepository>();
            if(authRepo.GetUserAuthByUserName("test") == null)
            {
                authRepo.CreateUserAuth(new CustoemUserAuth
                {
                    PersonalId = "1234567891",
                    Email = "test@test.com",
                    UserName = "test",
                    Roles = { "Admin" }
                }, "test");
            }
            JsConfig<DateTime>.SerializeFn = date => date.ToUniversalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            JsConfig<DateTime?>.SerializeFn = date => date.Value.ToUniversalTime().ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
        }

    }
}