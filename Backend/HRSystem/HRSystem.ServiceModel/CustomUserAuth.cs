﻿using ServiceStack.Auth;
using ServiceStack.DataAnnotations;
using System;


namespace HRSystem.ServiceModel
{
    public class CustoemUserAuth : UserAuth
    {
        [Unique]
        public string PersonalId { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? HiringDate { get; set; }
        public int? RemainingHoursOff { get; set; }
        public int? RemainingSickHoursOff { get; set; }
        public bool? Active { get; set; }
        public string Type { get; set; }
    }
}
