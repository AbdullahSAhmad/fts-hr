﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel.SystemEnum
{
    public enum UserTimeOffTypeEnum
    {
        Paid = 0,
        Sick = 1, 
        NotPaid = 2
    }
}
