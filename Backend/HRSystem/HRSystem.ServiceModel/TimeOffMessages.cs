﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel
{
    public class TimeOffMessages
    {
        [AutoIncrement]
        public int Id { get; set; }
        [References(typeof(UserTimeOffs))]
        public int UserTimeOffId { get; set; }
        [Reference]
        public UserTimeOffs UserTimeOff { get; set; }
        [References(typeof(CustoemUserAuth))]
        public int UserId { get; set; }

        [Reference]
        public CustoemUserAuth User { get; set; }
        public string Message { get; set; }
        public DateTime? DateOfMessage { get; set; }
        public int ActionId { get; set; }
    }

    public class TimeOffMessagesResponse
    {
        public int Id { get; set; }
        public int UserTimeOffId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
        public DateTime? DateOfMessage { get; set; }
        public string Action { get; set; }
    }
}


