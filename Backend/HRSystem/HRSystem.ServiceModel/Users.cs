﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel
{
    [Authenticate]
    [Route("/api/users", "Post")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class CreateUser : IReturn<UserResponse>
    {
        public string Password { get; set; }
        public string UserName { get; set; }
        public string City { get; set; }
        public string PersonalId { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? HiringDate { get; set; }
        public int? RemainingDaysOff { get; set; }
        public int? RemainingSickDaysOff { get; set; }
        public bool? Active { get; set; }
        public string Type { get; set; }
        public string FullName { get; set; }
    }


    [Authenticate]
    [Route("/api/users/{Id}", "Delete")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class DeleteUser : IReturnVoid
    {
        public int Id { get; set; }
    }

    [Authenticate]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    [Route("/api/users", "GET")]
    public class GetUsers : IReturn<List<UserResponse>>
    {

    }

    [Authenticate]
    [Route("/api/users/{Id}", "GET")]
    public class GetUserById : IReturn<UserResponse>
    {
        public int Id { get; set; }
    }

    [Route("/api/users/{Id}", "Put")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class UpdateUser : IReturn<UserResponse>
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string City { get; set; }
        public string PersonalId { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? HiringDate { get; set; }
        public int? RemainingDaysOff { get; set; }
        public int? RemainingSickDaysOff { get; set; }
        public bool? Active { get; set; }
        public string Type { get; set; }
        public string FullName { get; set; }
    }

    [Route("/api/users/shiftDaysOff", "Put")]
    public class ShiftDaysOff
    {
    }

    [Authenticate]
    [Route("/api/users/resetPassword/{Id}", "Put")]
    [RequiresAnyRole("Admin", "Manger")]
    public class ResetPassword : IReturnVoid
    {
        public int Id { get; set; }
        public string NewPassword { get; set; }
    }

    [Authenticate]
    [Route("/api/users/active/{Id}", "Post")]
    [Route("/api/users/active/{Id}", "Delete")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class ActiveUser : IReturnVoid
    {
        public int Id { get; set; }
    }

    [Authenticate]
    [Route("/api/users/changePassword", "Put")]
    public class ChangePassword : IReturnVoid
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class UserResponse
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string PersonalId { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? HiringDate { get; set; }
        public int? RemainingDaysOff { get; set; }
        public int? RemainingSickDaysOff { get; set; }
        public bool Active { get; set; }
        public string Type { get; set; }
        public string FullName { get; set; }
    }

}
