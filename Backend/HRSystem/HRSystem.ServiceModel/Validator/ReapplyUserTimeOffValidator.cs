﻿using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel.Validator
{
    class ReapplyUserTimeOffValidator : AbstractValidator<ReapplyUserTimeOff>
    {
        public ReapplyUserTimeOffValidator()
        {
            RuleFor(x => x.Message).NotEmpty();
        }
    }
}
