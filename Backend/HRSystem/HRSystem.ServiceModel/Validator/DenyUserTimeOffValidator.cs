﻿using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel.Validator
{
    class DenyUserTimeOffValidator : AbstractValidator<DenyUserTimeOff>
    {
        public DenyUserTimeOffValidator()
        {
            RuleFor(x => x.Message).NotEmpty();
        }
    }
}
