﻿using HRSystem.ServiceModel;
using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface.Validator
{
    public class CreateUserValidator : AbstractValidator<CreateUser>
    {
        public CreateUserValidator()
        {
            RuleFor(x => x.PersonalId).NotEmpty();
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Type).NotEmpty();
        }
    }
}
