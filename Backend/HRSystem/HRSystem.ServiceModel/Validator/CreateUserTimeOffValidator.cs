﻿using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel.Validator
{
    class CreateUserTimeOffValidator : AbstractValidator<CreateUserTimeOff>
    {
        public CreateUserTimeOffValidator()
        {
            RuleFor(c => c.StartDate).NotEmpty();
            RuleFor(c => c.EndDate).NotEmpty();
            RuleFor(c => c.Type).NotEmpty();
            RuleFor(c => c.Reason).NotEmpty();
        }
    }
}
