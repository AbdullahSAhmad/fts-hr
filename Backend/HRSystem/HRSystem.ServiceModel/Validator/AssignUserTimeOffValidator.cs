﻿using ServiceStack.FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel.Validator
{
    class AssignUserTimeOffValidator : AbstractValidator<AssignUserTimeOff>
    {
        public AssignUserTimeOffValidator()
        {
            RuleFor(c => c.ReviewerId).NotEmpty();
        }
    }
}
