﻿using ServiceStack;
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceModel
{
    public class UserTimeOffs
    {
        [AutoIncrement]
        public int Id { get; set; }
        [References(typeof(CustoemUserAuth))]
        [Required]
        public int? UserId { get; set; }
        [Reference]
        public CustoemUserAuth User { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int TypeId { get; set; }
        public string Reason { get; set; }
        [Required]
        [References(typeof(CustoemUserAuth))]
        public int? ReviewerId { get; set; }

        [Reference]
        public CustoemUserAuth Reviewer { get; set; }

        [Required]
        public int StatusId { get; set; }

        [Reference]
        public List<TimeOffMessages> Messages { get; set; }
    }
    [Authenticate]
    [Route("/api/timeOffs", "Post")]
    public class CreateUserTimeOff : IReturn<UserTimeOffsResponse>
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Type { get; set; }
        public string Reason { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs/assign/{Id}", "Put")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class AssignUserTimeOff : IReturnVoid
    {
        public int ReviewerId { get; set; }
        public int Id { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs/approve/{Id}", "Put")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class ApproveUserTimeOff : IReturnVoid
    {
        public int Id { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs/deny/{Id}", "Put")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class DenyUserTimeOff : IReturnVoid
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs/users/{UserId}", "Get")]
    public class GetUserTimeOffsByUserId : IReturn<List<UserTimeOffsWithDetailsResponse>>
    {
        public int UserId { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs", "Get")]
    [RequiresAnyRole("HR", "Admin", "Manger")]
    public class GetAllUserTimeOffs : IReturn<List<UserTimeOffsWithDetailsResponse>> { }

    [Authenticate]
    [Route("/api/timeOffs/{Id}", "Delete")]
    public class DeleteUserTimeOff : IReturnVoid
    {
        public int Id { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs/reapply/{Id}", "Put")]
    public class ReapplyUserTimeOff : IReturnVoid
    {
        public int Id { get; set; }
        public string Message { get; set; }

    }

    [Authenticate]
    [Route("/api/timeOffs/save/{Id}", "Put")]
    public class SaveUserTimeOff : IReturnVoid
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Type { get; set; }
    }

    [Authenticate]
    [Route("/api/timeOffs/{Id}", "Get")]
    public class GetUserTimeOffsById : IReturn<List<UserTimeOffsWithDetailsResponse>>
    {
        public int Id { get; set; }
        public string Meassage { get; set; }
    }

    public class UserTimeOffsResponse
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Type { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
    }
    public class UserTimeOffsWithDetailsResponse
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Type { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int ReviewerId { get; set; }
        public string ReviewerName { get; set; }
        public List<TimeOffMessagesResponse> Messages { get; set; }
        
    }


}
