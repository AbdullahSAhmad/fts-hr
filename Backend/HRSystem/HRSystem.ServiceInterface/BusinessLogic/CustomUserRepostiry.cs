﻿using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface.BusinessLogic
{
    public class CustomUserRepostiry : ICustomUserRepostiry
    {
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public IAuthRepository AuthRepository { get; set; }

        public void ActiveUser(int id, IRequest request)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var user = db.Single<CustoemUserAuth>(c => c.Id == id);
                if (user == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "can't find user with this Id = " + id);
                }
                CheckRoles(user, request);
                user.Active = true;
                db.Update(user);
            }
        }

        public void ChangePassword(string oldPassword, string newPassword, IRequest request)
        {
            var session = request.GetSession();
            var user = AuthRepository.GetUserAuth(session.UserAuthId);
            bool isCorrectPassword = user.VerifyPassword(oldPassword, out bool needsRehas);
            if (isCorrectPassword)
            {
                AuthRepository.UpdateUserAuth(user, user, newPassword);
            }
            else
            {
                throw new HttpError(HttpStatusCode.BadRequest, "your cuurent password is wrong ");
            }
        }

        public void CreateUser(CustoemUserAuth userAuth, string password)
        {
            AuthRepository.CreateUserAuth(userAuth, password);
        }

        public void DeactiveUser(int id, IRequest request)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var user = db.Single<CustoemUserAuth>(c => c.Id == id);
                if (user == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "can't find user with this Id = " + id);
                }
                CheckRoles(user, request);
                user.Active = false;
                db.Update(user);
            }
        }

        public void DeleteUser(int id, IRequest request)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var user = db.Single<CustoemUserAuth>(c => c.Id == id);
                if (user == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "can't find user with this Id = " + id);
                }
                CheckRoles(user, request);
                db.Delete<CustoemUserAuth>(c => c.Id == id);
            }
        }

        public CustoemUserAuth GetUserById(int id)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                return db.SingleById<CustoemUserAuth>(id);
            }
        }

        public List<CustoemUserAuth> GetUsers()
        {
            List<CustoemUserAuth> customeUsersAuth = null;
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                customeUsersAuth = db.Where<CustoemUserAuth>(true).ToList();
            }
            return customeUsersAuth;
        }

        public void ResetPassword(int id, string password)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var user = db.SingleById<CustoemUserAuth>(id);
                if (user == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "can't find user with this Id = " + id);
                }

                AuthRepository.UpdateUserAuth(user, user, password);
            }
        }

        public void ShiftDaysOffServices()
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var customeUsersAuth = db.Where<CustoemUserAuth>(true).ToList();
                customeUsersAuth.ForEach(c =>
                {
                    c.RemainingHoursOff = c.RemainingHoursOff + 48 > 160 ? 160 : c.RemainingHoursOff + 48;
                    c.RemainingSickHoursOff = 112;
                    db.Update<CustoemUserAuth>(c);
                });
            }
        }

        public CustoemUserAuth UpdateUser(UpdateUser updateUser, IRequest request)
        {
            CustoemUserAuth userAuth = updateUser.ConvertTo<CustoemUserAuth>();
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var user = db.Single<CustoemUserAuth>(c => c.Id == updateUser.Id);
                if (user == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "can't find user with this Id = " + updateUser.Id);
                }
                CheckRoles(userAuth, request);
                if (!userAuth.Roles[0].Equals(updateUser.Type))
                {
                    userAuth.Roles[0] = updateUser.Type;
                    CheckRoles(userAuth, request);
                }
                db.UpdateNonDefaults<CustoemUserAuth>(userAuth, c => c.Id == updateUser.Id);
                userAuth = db.SingleById<CustoemUserAuth>(updateUser.Id);
            }
            return userAuth;
        }

        private void CheckRoles(CustoemUserAuth user, IRequest request)
        {
            if (user.Roles.SingleOrDefault(r => r.Equals("Employee")) == null)
            {
                var session = request.GetSession();
                if (!(session.HasRole("Admin", AuthRepository) || session.HasRole("Manger", AuthRepository)))
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "you don't hace permissions to add user from this type " + user.Roles.ToString());
                }
            }
        }
    }
}
