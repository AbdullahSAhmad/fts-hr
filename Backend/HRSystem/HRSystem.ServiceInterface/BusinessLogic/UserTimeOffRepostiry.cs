﻿using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using HRSystem.ServiceModel.SystemEnum;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface.BusinessLogic
{
    public class UserTimeOffRepostiry : IUserTimeOffRepostiry
    {
        public IDbConnectionFactory DbConnectionFactory { get; set; }
        public ICustomUserRepostiry CustomUserRepostiry { get; set; }

        public void ApproveUserTimeOff(int id, int currentUserId)
        {
            UserTimeOffs userTimeOff = null;

            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                userTimeOff = db.SingleById<UserTimeOffs>(id);
                if (userTimeOff == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "there this no user time off with this id : " + id);
                }
                
                if (userTimeOff.ReviewerId == currentUserId)
                {
                    var user = CustomUserRepostiry.GetUserById(userTimeOff.UserId.Value);
                    if(userTimeOff.StatusId == (int)UserTimeOffStatusEnum.Approved)
                    {
                        throw new HttpError(HttpStatusCode.MethodNotAllowed, "this time off is already appoved");
                    }
                    if(DateTime.Compare(userTimeOff.StartDate.Value.Date, DateTime.Now.Date) < 0)
                    {
                        throw new HttpError(HttpStatusCode.MethodNotAllowed, "you can't approve this time off because it has old date");
                    }
                    int timeOffHours = GetNumberOfWorkingDays(userTimeOff.StartDate.Value, userTimeOff.EndDate.Value) * 8; 
                    if(userTimeOff.TypeId == (int)UserTimeOffTypeEnum.Paid)
                    {
                        if(user.RemainingHoursOff.Value >= timeOffHours)
                        {
                            user.RemainingHoursOff = user.RemainingHoursOff.Value - timeOffHours;
                        }
                        else
                        {
                            throw new HttpError(HttpStatusCode.MethodNotAllowed, "you don't have enough days");
                        }
                    }
                    else if(userTimeOff.TypeId == (int)UserTimeOffTypeEnum.Sick)
                    {
                        if(user.RemainingSickHoursOff.Value >= timeOffHours)
                        {
                            user.RemainingSickHoursOff = user.RemainingSickHoursOff.Value - timeOffHours;
                        }
                        else
                        {
                            throw new HttpError(HttpStatusCode.MethodNotAllowed, "you don't have enough days");
                        }
                    } 
                    db.Update<CustoemUserAuth>(user);
                    userTimeOff.StatusId = (int)UserTimeOffStatusEnum.Approved;
                    db.UpdateNonDefaults(userTimeOff, t => t.Id == id);
                }
                else
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "this time off not assigned to you so you can't approve ");
                }
            }
        }

        public void AssignUserTimeOff(int id, int newReviwerId)
        {
            UserTimeOffs userTimeOff = null;

            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                userTimeOff = db.SingleById<UserTimeOffs>(id);
                if (userTimeOff == null)
                {
                    throw new HttpError(HttpStatusCode.NotFound, "there this no user time off with this id : " + id);
                }
                userTimeOff.ReviewerId = newReviwerId;
                db.UpdateNonDefaults(userTimeOff, t => t.Id == id);
            }
        }

        public UserTimeOffs CreateUserTimeOff(UserTimeOffs userTimeOff)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var user = CustomUserRepostiry.GetUserById(userTimeOff.UserId.Value);
                if (DateTime.Compare(userTimeOff.StartDate.Value.Date, DateTime.Now.Date) < 0)
                {
                    throw new HttpError(HttpStatusCode.BadRequest, "can't create user time off with this date");
                }
                if (DateTime.Compare(userTimeOff.StartDate.Value.Date, userTimeOff.EndDate.Value.Date) >= 0)
                {
                    throw new HttpError(HttpStatusCode.BadRequest, "can't create user time off with this date");
                }
                int timeOffHours = GetNumberOfWorkingDays(userTimeOff.StartDate.Value, userTimeOff.EndDate.Value) * 8;
                if (userTimeOff.TypeId == (int)UserTimeOffTypeEnum.Paid)
                {
                    if (user.RemainingHoursOff.Value < timeOffHours)
                    {
                        throw new HttpError(HttpStatusCode.MethodNotAllowed, "you don't have enough days");
                    }
                }
                else if (userTimeOff.TypeId == (int)UserTimeOffTypeEnum.Sick)
                {
                    if (user.RemainingSickHoursOff.Value < timeOffHours)
                    {
                        throw new HttpError(HttpStatusCode.MethodNotAllowed, "you don't have enough days");
                    }
                }
                long id = db.Insert(userTimeOff, selectIdentity: true);
                userTimeOff = db.LoadSingleById<UserTimeOffs>(id);
            }
            return userTimeOff;
        }

        public void DeleteTimeOff(int id, int currentUserId)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var userTimeOff = db.SingleById<UserTimeOffs>(id);
                if (userTimeOff == null)
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "There this no user time off with this id : " + id);
                }
                if (db.SingleById<UserTimeOffs>(userTimeOff.UserId).ReviewerId == currentUserId)
                {
                    db.DeleteById<UserTimeOffs>(userTimeOff.Id);
                }
                else
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "You are not the owner for this time off so you can't delete it");
                }
            }
        }

        public void DenyUserTimeOff(int id, string message, int currentUserId)
        {
            TimeOffMessages timeOffMessage = null;
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var userTimeOff = db.SingleById<UserTimeOffs>(id);

                if (userTimeOff == null)
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "There this no user time off with this id : " + id);
                }
                if(userTimeOff.StatusId == (int)UserTimeOffStatusEnum.Approved)
                {
                    throw new HttpError(HttpStatusCode.MethodNotAllowed, "You can't deny this user time off because it's approved");
                }
                if (userTimeOff.StatusId == (int)UserTimeOffStatusEnum.Denied)
                {
                    throw new HttpError(HttpStatusCode.MethodNotAllowed, "this user time off is already denied");
                }

                if (userTimeOff.ReviewerId == currentUserId)
                {
                    timeOffMessage = new TimeOffMessages()
                    {
                        Message = message,
                        ActionId = (int)TimeOffMessagesActionEnum.Deny,
                        DateOfMessage = DateTime.Now,
                        UserTimeOffId = userTimeOff.Id,
                        UserId = currentUserId
                    };
                    db.Insert<TimeOffMessages>(timeOffMessage);
                    userTimeOff.StatusId = (int)UserTimeOffStatusEnum.Denied;
                    db.UpdateNonDefaults(userTimeOff, t => t.Id == id);
                }
                else
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "this time off not assigned to you so you can't deny ");
                }
            }
        }

        public List<UserTimeOffs> GetAllUserTimeOffs()
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var results = db.From<UserTimeOffs>();
                return db.LoadSelect(results);
            }
        }

        public UserTimeOffs GetUserTimeOffById(int id, int currentUserId)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                UserTimeOffs userTimeOff = db.LoadSingleById<UserTimeOffs>(id);
                if (userTimeOff == null)
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "There this no user time off with this id : " + id);
                }
                var currentUser = CustomUserRepostiry.GetUserById(currentUserId);

                if (userTimeOff.UserId == currentUserId || userTimeOff.ReviewerId == currentUserId || currentUser.Type.Equals("HR") || currentUser.Type.Equals("Admin"))
                {
                    return userTimeOff;
                }
                else
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, " you not authorized to see this user time off");
                }
            }
        }

        public List<UserTimeOffs> GetUserTimeOffsByUserId(int userId)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var results = db.From<UserTimeOffs>().Where(u => u.UserId == userId);
                var userTimeOffs = db.LoadSelect(results);
                return userTimeOffs;
            }
        }

        public void UpdateUserTimeOff(UserTimeOffs userTimeOff, int currentUserId)
        {

            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                if(db.SingleById<UserTimeOffs>(userTimeOff.Id).UserId != currentUserId)
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, " you not authorized to update this user time off");
                }
                db.UpdateNonDefaults(userTimeOff, t => t.Id == userTimeOff.Id);
            }
                
        }

        public void ReapplyUserTimeOff(int id, string message, int currentUserId)
        {
            TimeOffMessages timeOffMessage = null;
            using (var db = DbConnectionFactory.OpenDbConnection())
            {
                var userTimeOff = db.SingleById<UserTimeOffs>(id);

                if (userTimeOff == null)
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "There this no user time off with this id : " + id);
                }
                if (userTimeOff.StatusId == (int)UserTimeOffStatusEnum.Approved)
                {
                    throw new HttpError(HttpStatusCode.MethodNotAllowed, "You can't requested this user time off because it's approved");
                }
                if ( userTimeOff.StatusId == (int)UserTimeOffStatusEnum.Requested)
                {
                    throw new HttpError(HttpStatusCode.MethodNotAllowed, "this user time off is already requested");
                }

                if (userTimeOff.UserId == currentUserId)
                {
                    timeOffMessage = new TimeOffMessages()
                    {
                        Message = message,
                        ActionId = (int)TimeOffMessagesActionEnum.Reapply,
                        DateOfMessage = DateTime.Now,
                        UserTimeOffId = userTimeOff.Id,
                        UserId = currentUserId
                    };
                    db.Insert<TimeOffMessages>(timeOffMessage);
                    userTimeOff.StatusId = (int)UserTimeOffStatusEnum.Requested;
                    db.Update(userTimeOff, t => t.Id == id);
                }
                else
                {
                    throw new HttpError(HttpStatusCode.Unauthorized, "this time off not assigned to you so you can't deny ");
                }
            }
        }
        private static int GetNumberOfWorkingDays(DateTime start, DateTime stop)
        {
            int days = 0;
            while (start <= stop)
            {
                if (start.DayOfWeek != DayOfWeek.Friday && start.DayOfWeek != DayOfWeek.Saturday)
                {
                    ++days;
                }
                start = start.AddDays(1);
            }
            return days;
        }
       
        
    }
}
