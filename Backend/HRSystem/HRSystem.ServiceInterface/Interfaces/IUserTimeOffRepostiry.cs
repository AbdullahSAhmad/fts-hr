﻿using HRSystem.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface.Interfaces
{
    public interface IUserTimeOffRepostiry 
    {
        UserTimeOffs CreateUserTimeOff(UserTimeOffs userTimeOff);
        void AssignUserTimeOff(int id, int newReviwerId);
        void ApproveUserTimeOff(int id, int currentUserId);
        void DenyUserTimeOff(int id, string message, int currentUserId);
        void DeleteTimeOff(int id, int currentUserId);
        UserTimeOffs GetUserTimeOffById(int id, int currentUserId);
        List<UserTimeOffs> GetUserTimeOffsByUserId(int userId);
        List<UserTimeOffs> GetAllUserTimeOffs();
        void UpdateUserTimeOff(UserTimeOffs userTimeOff, int currentUserId);
        void ReapplyUserTimeOff(int id, string message, int currentUserId);
    }
}
