﻿using HRSystem.ServiceModel;
using ServiceStack.Auth;
using ServiceStack.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface.Interfaces
{
    public interface ICustomUserRepostiry
    {
        void CreateUser(CustoemUserAuth userAuth, string password);
        CustoemUserAuth UpdateUser(UpdateUser updateUser, IRequest request);
        void DeleteUser(int id, IRequest request);
        List<CustoemUserAuth> GetUsers();
        CustoemUserAuth GetUserById(int id);
        void ChangePassword(string oldPassword, string newPassword, IRequest request);
        void ResetPassword(int id, string password);
        void ShiftDaysOffServices();
        void ActiveUser(int id, IRequest request);
        void DeactiveUser(int id, IRequest request);
    }
}
