﻿using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using HRSystem.ServiceModel.SystemEnum;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface
{
    public class UserTimeOffsServices : Service
    {
        public static string req = "Requested";
        public static string Approved = "Approved";
        public static string Denied = "Denied";

        public IAuthRepository AuthRepository { get; set; }
        public ICustomUserRepostiry CustomUserRepostiry { get; set; }
        public IUserTimeOffRepostiry UserTimeOffRepostiry { get; set; }
        public object Post(CreateUserTimeOff createUserTimeOff)
        {
            var req = this.Request;
            var session = req.GetSession();
            var userTimeOff = createUserTimeOff.ConvertTo<UserTimeOffs>();

            userTimeOff.StatusId = (int)UserTimeOffStatusEnum.Requested;
            userTimeOff.UserId = int.Parse(session.UserAuthId);
            Enum.TryParse(createUserTimeOff.Type, out UserTimeOffTypeEnum type);

            userTimeOff.TypeId = (int)type;
            var reviewer = CustomUserRepostiry.GetUsersByRole("HR");
            if(reviewer == null || reviewer.Count == 0)
            {
                reviewer = CustomUserRepostiry.GetUsersByRole("Admin");
            }
            userTimeOff.ReviewerId = reviewer[0].Id;
            userTimeOff = UserTimeOffRepostiry.CreateUserTimeOff(userTimeOff);
            var userTimeOffsResponse = userTimeOff.ConvertTo<UserTimeOffsResponse>();
            userTimeOffsResponse.Type = ((UserTimeOffTypeEnum)userTimeOff.TypeId).ToString();
            userTimeOffsResponse.Status = ((UserTimeOffStatusEnum)userTimeOff.TypeId).ToString();
            return userTimeOffsResponse; 
        }

        public void Put(AssignUserTimeOff assignUserTimeOff)
        {
            UserTimeOffRepostiry.AssignUserTimeOff(assignUserTimeOff.Id, assignUserTimeOff.ReviewerId);
        } 
        public void Put(ApproveUserTimeOff approveUserTimeOff)
        {
            var currentUserId = int.Parse(this.Request.GetSession().UserAuthId);
            UserTimeOffRepostiry.ApproveUserTimeOff(approveUserTimeOff.Id, currentUserId);
        }

        public void Put(DenyUserTimeOff denyUserTimeOff)
        {
            var currentUserId = int.Parse(this.Request.GetSession().UserAuthId);
            UserTimeOffRepostiry.DenyUserTimeOff(denyUserTimeOff.Id, denyUserTimeOff.Message, currentUserId);
        }

        public object Get(GetUserTimeOffsByUserId getUserTimeOffsByUserId)
        {
            var session = this.Request.GetSession();
            if ((getUserTimeOffsByUserId.UserId != int.Parse(session.UserAuthId)) && (session.HasRole("Employee", AuthRepository)))
            {
                throw new HttpError(HttpStatusCode.Unauthorized, "you can't see these user time offs");
            }
            return ConvertListFromUserTimeOffsToListUserTimeOffsWithDetailsResponse(UserTimeOffRepostiry.GetUserTimeOffsByUserId(getUserTimeOffsByUserId.UserId));
        }

        public object Get(GetAllUserTimeOffs getAllUserTimeOffs)
        {
            return ConvertListFromUserTimeOffsToListUserTimeOffsWithDetailsResponse(UserTimeOffRepostiry.GetAllUserTimeOffs());
        }

        public void Delete(DeleteUserTimeOff deleteUserTimeOff)
        {
            UserTimeOffRepostiry.DeleteTimeOff(deleteUserTimeOff.Id, int.Parse(this.Request.GetSession().UserAuthId));
        }
        public object Get(GetUserTimeOffsById getUserTimeOffsById)
        {   
            return ConvertFromUserTimeOffsToUserTimeOffsWithDetailsResponse(UserTimeOffRepostiry.GetUserTimeOffById(getUserTimeOffsById.Id, int.Parse(this.Request.GetSession().UserAuthId)));
        }
        public void Put(SaveUserTimeOff saveUserTimeOff)
        {
            var userTimeOff = saveUserTimeOff.ConvertTo<UserTimeOffs>();
            UserTimeOffRepostiry.UpdateUserTimeOff(userTimeOff, int.Parse(this.Request.GetSession().UserAuthId));
        }

        public void Put(ReapplyUserTimeOff reapplyUserTimeOff)
        {
            var currentUserId = int.Parse(this.Request.GetSession().UserAuthId);
            UserTimeOffRepostiry.ReapplyUserTimeOff(reapplyUserTimeOff.Id, reapplyUserTimeOff.Message, currentUserId);
        }
        private List<UserTimeOffsWithDetailsResponse> ConvertListFromUserTimeOffsToListUserTimeOffsWithDetailsResponse(List<UserTimeOffs> userTimeOffs)
        {
            var userTimeOffsWithDetailsResponses = new List<UserTimeOffsWithDetailsResponse>(); 
            foreach (var userTimeOff in userTimeOffs)
            {
                var userTimeOffsWithDetailsResponse = ConvertFromUserTimeOffsToUserTimeOffsWithDetailsResponse(userTimeOff);

                userTimeOffsWithDetailsResponses.Add(userTimeOffsWithDetailsResponse);
            }
            return userTimeOffsWithDetailsResponses;
        }
        private UserTimeOffsWithDetailsResponse ConvertFromUserTimeOffsToUserTimeOffsWithDetailsResponse(UserTimeOffs userTimeOff)
        {

            var userTimeOffsWithDetailsResponse = userTimeOff.ConvertTo<UserTimeOffsWithDetailsResponse>();
            userTimeOffsWithDetailsResponse.UserName = userTimeOff.User.UserName;
            userTimeOffsWithDetailsResponse.ReviewerName = userTimeOff.Reviewer.UserName;
            userTimeOffsWithDetailsResponse.Type = ((UserTimeOffTypeEnum)userTimeOff.TypeId).ToString();
            userTimeOffsWithDetailsResponse.Status = ((UserTimeOffStatusEnum)userTimeOff.StatusId).ToString();
            if (userTimeOff.Messages != null && userTimeOff.Messages.Count() != 0)
            {
                var listOfTimeOffMessagesResponse = new List<TimeOffMessagesResponse>();
                foreach (var timeOffMessages in userTimeOff.Messages)
                {
                    TimeOffMessagesResponse timeOffMessagesResponse = timeOffMessages.ConvertTo<TimeOffMessagesResponse>();
                    timeOffMessagesResponse.Action = ((TimeOffMessagesActionEnum)timeOffMessages.ActionId).ToString();
                    timeOffMessagesResponse.UserName = CustomUserRepostiry.GetUserById(timeOffMessages.UserId).UserName;
                    listOfTimeOffMessagesResponse.Add(timeOffMessagesResponse);
                }
                userTimeOffsWithDetailsResponse.Messages = listOfTimeOffMessagesResponse;
            }
            return userTimeOffsWithDetailsResponse;
        }
    }
}
