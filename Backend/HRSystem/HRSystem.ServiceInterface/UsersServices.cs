﻿using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ServiceInterface
{
    public class UsersServices : Service
    {
        public ICustomUserRepostiry CustomUserRepostiry { get; set; }
        [AddHeader(HttpStatusCode.Created)]
        public object Post(CreateUser createUser)
        {
            CustoemUserAuth user = createUser.ConvertTo<CustoemUserAuth>();
            if (!string.IsNullOrWhiteSpace(createUser.Type))
            {
                if (!createUser.Type.Equals("Employee"))
                {
                    var req = this.Request;
                    var session = req.GetSession();
                     if (!(session.HasRole("Admin", AuthRepository) || session.HasRole("Manger", AuthRepository)))
                    {
                        throw new HttpError(HttpStatusCode.Unauthorized, "you don't hace permissions to add user from this type " + createUser.Type);
                    }
                }
                user.Roles.Add(createUser.Type);
            }
            else
            {
                throw new HttpError(HttpStatusCode.BadRequest, "please enter all information");
            }
            user.RemainingHoursOff = createUser.RemainingDaysOff != null ? createUser.RemainingDaysOff * 8 : 112;
            user.RemainingSickHoursOff = createUser.RemainingSickDaysOff != null ? createUser.RemainingSickDaysOff * 8 : 112;
            if (user.Active == null)
            {
                user.Active = true;
            }
            if(user.Type == null)
            {
                user.Type = "Employee";
            }
            CustomUserRepostiry.CreateUser(user, createUser.Password);
            var userRes = user.ConvertTo<UserResponse>();
            userRes.Type = createUser.Type;
            userRes.RemainingDaysOff = user.RemainingHoursOff / 8;
            userRes.RemainingSickDaysOff = user.RemainingSickHoursOff / 8;
            return userRes;
        }

        public object Put(UpdateUser updateUser)
        {
            var customeUserAuth = CustomUserRepostiry.UpdateUser(updateUser, this.Request);
            var userRes = customeUserAuth.ConvertTo<UserResponse>();
            if (customeUserAuth.Roles.Count != 0)
            {
                userRes.Type = customeUserAuth.Roles[0];
            }
            userRes.RemainingDaysOff = customeUserAuth.RemainingHoursOff / 8;
            userRes.RemainingSickDaysOff = customeUserAuth.RemainingSickHoursOff / 8;
            return userRes;
        }

        public void Delete(DeleteUser deleteUser)
        {
            CustomUserRepostiry.DeleteUser(deleteUser.Id, this.Request);
        }

        public object Get(GetUsers getUsers)
        {
            List<UserResponse> users = new List<UserResponse>();
            var customeUsersAuth = CustomUserRepostiry.GetUsers();
            customeUsersAuth.ForEach(c =>
            {
                var user = c.ConvertTo<UserResponse>();
                if (c.Roles.Count != 0)
                {
                    user.Type = c.Roles[0];
                }
                user.RemainingDaysOff = c.RemainingHoursOff / 8;
                user.RemainingSickDaysOff = c.RemainingSickHoursOff / 8;
                users.Add(user);
            });
            return users;
        }

        public object Get(GetUserById getUserById)
        {   
            UserResponse userRes;
            var customeUserAuth = CustomUserRepostiry.GetUserById(getUserById.Id);
            if(customeUserAuth == null)
            {
                throw new HttpError(HttpStatusCode.NotFound, "can't find user with this Id");
            }
            userRes = customeUserAuth.ConvertTo<UserResponse>();
            if (customeUserAuth.Roles.Count != 0)
            {
                userRes.Type = customeUserAuth.Roles[0];
            }
            userRes.RemainingDaysOff = customeUserAuth.RemainingHoursOff / 8;
            userRes.RemainingSickDaysOff = customeUserAuth.RemainingSickHoursOff / 8;
            return userRes;
        }

        public void Put(ChangePassword changePassword)
        {
            CustomUserRepostiry.ChangePassword(changePassword.OldPassword, changePassword.NewPassword, this.Request);
        }

        public void Put(ResetPassword resetPassword)
        {
            CustomUserRepostiry.ResetPassword(resetPassword.Id, resetPassword.NewPassword);
        }

        public void Post(ActiveUser activeUser)
        {
            CustomUserRepostiry.ActiveUser(activeUser.Id, this.Request);
        }
        public void Delete(ActiveUser activeUser)
        {
            CustomUserRepostiry.DeactiveUser(activeUser.Id, this.Request);
        }

        public void Put(ShiftDaysOff shiftDaysOff)
        {
            DateTime dateTime = DateTime.UtcNow.Date;
            if (dateTime.Day == 1 && dateTime.Month == 1)
            {
                CustomUserRepostiry.ShiftDaysOffServices();
            }
        }

    }
}
