﻿using HRSystem.ServiceInterface;
using HRSystem.ServiceModel;
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.Tests
{
    class UserTimeOffsServicesUnitTest : BasicUnitTest
    {
        UserTimeOffsServices service;

        [OneTimeSetUp]
        public void SetupUserTimeOffsServices()
        {
            service = appHost.Container.Resolve<UserTimeOffsServices>();
            var req = new MockHttpRequest();
            req.Items[Keywords.Session] = new AuthUserSession();
            service.Request = req;
        }

        [Test]
        public void CanCreateUserTimeOffs()
        {
            
            var reason = "I want to visit my uncle";
            var response = (UserTimeOffsResponse)service.Post(new CreateUserTimeOff
            {
                EndDate = new DateTime(2019, 1, 22),
                StartDate = new DateTime(2019, 1, 21),
                Reason = reason,
                Type = "Paid"
            });
            Assert.True(response.Reason.Equals(reason));
            Assert.True(response.Type.Equals("Paid"));
        }
        [Test]
        public void CanAssignUserTimeOffs()
        {
            service.Put(new AssignUserTimeOff
            {
                Id = 1,
                ReviewerId = 1
            });
            Assert.True(true);
        }
        [Test]
        public void CanApproveUserTimeOffs()
        {
            service.Put(new ApproveUserTimeOff
            {
                Id = 1,
            });
            Assert.True(true);
        }

        [Test]
        public void CanDenyUserTimeOffs()
        {
            service.Put(new DenyUserTimeOff
            {
                Id = 1,
                Message = "test "
            });
            Assert.True(true);
        }

        [Test]
        public void CanDeleteUserTimeOffs()
        {
            service.Delete(new DeleteUserTimeOff
            {
                Id = 1,
            });
            Assert.True(true);
        }

        [Test]
        public void CanReapplyUserTimeOffs()
        {
            service.Put(new ReapplyUserTimeOff
            {
                Id = 1,
                Message = "test test"
            });
            Assert.True(true);
        }
        [Test]
        public void CanGetByIdUserTimeOffs()
        {
            var response = (UserTimeOffsWithDetailsResponse)service.Get(new GetUserTimeOffsById
            {
                Id = 1
            });
            Assert.True(response.Id == 1);
        }

        [Test]
        public void CanGetByUserIdUserTimeOffs()
        {
            var response = (List<UserTimeOffsWithDetailsResponse>)service.Get(new GetUserTimeOffsByUserId
            {
                UserId = 1
            });
            Assert.True(response.Count == 2);
        }

        [Test]
        public void CanGetAllUserTimeOffs()
        {
            var response = (List<UserTimeOffsWithDetailsResponse>)service.Get(new GetAllUserTimeOffs());
            Assert.True(response.Count == 2);
        }
        [Test]
        public void CanSaveUserTimeOffs()
        {
            var reason = "test2";

            service.Put(new SaveUserTimeOff
            {
                Id = 1,
                EndDate = new DateTime(2019, 1, 22),
                StartDate = new DateTime(2019, 1, 21),
                Reason = reason,
                Type = "Paid"
            });
        }
    }
}
