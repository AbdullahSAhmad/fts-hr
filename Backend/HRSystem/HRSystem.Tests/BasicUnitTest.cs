﻿using HRSystem.ServiceInterface;
using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using HRSystem.Tests.MockRepostirys;
using HRSystem.Tests.RepositoriesMock;
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Testing;
using System.Collections.Generic;


namespace HRSystem.Tests
{
    [TestFixture]
    public class BasicUnitTest
    {
        public ServiceStackHost appHost;
        public static AuthUserSession CreateUserSession()
        {
            var Roles = new List<string>();
            Roles.Add("Admin");
            return new AuthUserSession
            {
                UserAuthId = "1",
                Language = "en",
                PhoneNumber = "*****",
                FirstName = "Test",
                LastName = "User",
                PrimaryEmail = "test@email.com",
                UserAuthName = "Mocked",
                UserName = "Mocked",
                Roles = Roles
            };
        }
        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            appHost.Container.AddTransient<UsersServices>();
            appHost.Container.AddTransient<UserTimeOffsServices>();
            var container = appHost.Container;
            container.Register<IAuthSession>(CreateUserSession());
            container.Register<ICustomUserRepostiry>(new CustomUserRepostiryMock());
            container.Register<IUserTimeOffRepostiry>(new UserTimeOffRepostiryMock());
            AuthUserSession authUserSession = new AuthUserSession();
        }
        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

    }
}
