﻿using NUnit.Framework;
using ServiceStack;
using ServiceStack.Testing;
using HRSystem.ServiceInterface;
using HRSystem.ServiceModel;
using System.Collections.Generic;
using ServiceStack.Auth;
using ServiceStack.OrmLite;
using ServiceStack.Data;
using ServiceStack.Web;
using HRSystem.ServiceInterface.Interfaces;
using HRSystem.Tests.MockRepostirys;

namespace HRSystem.Tests
{
    [TestFixture]
    public class UserServicesUnitTest
    {
        private ServiceStackHost appHost;
        public static AuthUserSession CreateUserSession()
        {
            var Roles = new List<string>();
            Roles.Add("Admin");
            return new AuthUserSession
            {
                UserAuthId = "1",
                Language = "en",
                PhoneNumber = "*****",
                FirstName = "Test",
                LastName = "User",
                PrimaryEmail = "test@email.com",
                UserAuthName = "Mocked",
                UserName = "Mocked",
                Roles = Roles
            };
        }

        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            appHost.Container.AddTransient<MyServices>();
            appHost.Container.AddTransient<UsersServices>();
            var container = appHost.Container;
            container.Register<IAuthSession>(CreateUserSession());
            container.Register<ICustomUserRepostiry>(new CustomUserRepostiryMock());

            AuthUserSession authUserSession = new AuthUserSession();

        }

        IServiceClient GetClientWithUserPassword()
        {
            return new JsonServiceClient("http://localhost:59758/")
            {
                UserName = "rooz",
                Password = "yazan"
            };
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();



        [Test]
        public void Can_Create_User()
        {
            var client = GetClientWithUserPassword();

            var service = appHost.Container.Resolve<UsersServices>();
            var req = new MockHttpRequest();
            req.Items[Keywords.Session] = new AuthUserSession();
            service.Request = req;
            var response = (UserResponse)service.Post(new CreateUser
            {
                PersonalId = "123456782",
                UserName = "khk",
                Password = "yazan",
                Email = "khk@hotmail.com",
                Active = true,
                FullName = "rooz wanas shawahni",
                Type = "HR",
                RemainingDaysOff = 14
            });
            Assert.True(response.PersonalId == "123456782");
        }

        [Test]
        public void Can_Update_User()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            var response = (UserResponse)service.Put(new UpdateUser
            {
                Id = 2,
                UserName = "test1",

            });
            Assert.True(response.UserName.Equals("test1"));
        }

        [Test]
        public void Can_Delete_User()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            service.Delete(new DeleteUser
            {
                Id = 2
            });
        }
        
        [Test]
        public void Can_Active_User()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            service.Post(new ActiveUser
            {
                Id = 2
            });
        }

        [Test]
        public void Can_Deactive_User()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            service.Delete(new ActiveUser
            {
                Id = 2
            });
        }
        [Test]
        public void Can_Get_UserById()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            var response = (UserResponse)service.Get(new GetUserById
            {
                Id = 2,
            });
            Assert.True(response.UserName.Equals("test"));
            Assert.True(response.Email.Equals("test@test.com"));
            Assert.True(response.PersonalId.Equals("1234567891"));
            Assert.True(response.Id == 2);

        }

        [Test]
        public void Can_Get_Users()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            var response = (List<UserResponse>)service.Get(new GetUsers());
            Assert.True(response.Count == 2);
            Assert.True(response[0].Id == 2);
            Assert.True(response[1].Id == 1);
        }
        [Test]
        public void Can_Call_ChangePassword()
        {
            var service = appHost.Container.Resolve<UsersServices>();

            service.Put(new ChangePassword
            {
                OldPassword = "test",
                NewPassword = "test1",
            });

        }
        [Test]
        public void Can_Call_ResetPassword()
        {
            var service = appHost.Container.Resolve<UsersServices>();
            service.Put(new ResetPassword
            {
                NewPassword = "test1",
                Id = 2
            });
          
        }
        [Test]
        public void Can_Call_ShiftDaysOffServices()
        {
            var service = appHost.Container.Resolve<UsersServices>();
            service.Put(new ShiftDaysOff());
        }
    }
}
