﻿using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.Tests.RepositoriesMock
{
    class UserTimeOffRepostiryMock : IUserTimeOffRepostiry
    {
        public void ApproveUserTimeOff(int id, int currentUserId)
        {

        }

        public void AssignUserTimeOff(int id, int newReviwerId)
        {
            
        }

        public UserTimeOffs CreateUserTimeOff(UserTimeOffs userTimeOff)
        {
            var employeeRoles = new List<string>();
            employeeRoles.Add("Employee");
            var mangerRoles = new List<string>();
            mangerRoles.Add("Maneger");
            return new UserTimeOffs
            {
                Id = 1,
                Reason = "I want to visit my uncle",
                StatusId = 0, 
                TypeId = 0,
                EndDate = new DateTime(2020, 1, 22),
                StartDate = new DateTime(2020, 1, 21),
                ReviewerId = 2,
                UserId = 1,
                User = new CustoemUserAuth
                {
                    Id = 1,
                    Email = "test1@test.com",
                    UserName = "test1",
                    PersonalId = "1234567891",
                    Roles = employeeRoles
                },
                Reviewer = new CustoemUserAuth
                {
                    Id = 2,
                    Email = "test@test.com",
                    UserName = "test",
                    PersonalId = "1234567890",
                    Roles = mangerRoles
                },
            };
        }

        public void DeleteTimeOff(int id, int currentUserId)
        {
            
        }

        public void DenyUserTimeOff(int id, string message, int currentUserId)
        {
            
        }

        public List<UserTimeOffs> GetAllUserTimeOffs()
        {
            var employeeRoles = new List<string>();
            employeeRoles.Add("Employee");
            var mangerRoles = new List<string>();
            mangerRoles.Add("Maneger");
            var userTimeOffs = new List<UserTimeOffs>();
            userTimeOffs.Add(new UserTimeOffs
            {
                Id = 1,
                Reason = "I want to visit my uncle",
                StatusId = 0,
                TypeId = 0,
                EndDate = new DateTime(2020, 1, 22),
                StartDate = new DateTime(2020, 1, 21),
                ReviewerId = 2,
                UserId = 1,
                User = new CustoemUserAuth
                {
                    Id = 1,
                    Email = "test1@test.com",
                    UserName = "test1",
                    PersonalId = "1234567891",
                    Roles = employeeRoles
                },
                Reviewer = new CustoemUserAuth
                {
                    Id = 2,
                    Email = "test@test.com",
                    UserName = "test",
                    PersonalId = "1234567890",
                    Roles = mangerRoles
                },
            });
            userTimeOffs.Add(new UserTimeOffs
            {
                Id = 2,
                Reason = "I want to take the reset",
                StatusId = 1,
                TypeId = 1,
                EndDate = new DateTime(2020, 1, 24),
                StartDate = new DateTime(2020, 1, 22),
                ReviewerId = 3,
                UserId = 1,
                User = new CustoemUserAuth
                {
                    Id = 1,
                    Email = "test1@test.com",
                    UserName = "test1",
                    PersonalId = "1234567891",
                    Roles = employeeRoles
                },
                Reviewer = new CustoemUserAuth
                {
                    Id = 3,
                    Email = "test@test.com",
                    UserName = "test",
                    PersonalId = "1234567892",
                    Roles = mangerRoles
                },
            });
            return userTimeOffs;
        }

        public UserTimeOffs GetUserTimeOffById(int id, int currentUserId)
        {
            var employeeRoles = new List<string>();
            employeeRoles.Add("Employee");
            var mangerRoles = new List<string>();
            mangerRoles.Add("Maneger");
            return new UserTimeOffs
            {
                Id = 1,
                Reason = "I want to visit my uncle",
                StatusId = 0,
                TypeId = 0,
                EndDate = new DateTime(2020, 1, 22),
                StartDate = new DateTime(2020, 1, 21),
                ReviewerId = 2,
                UserId = 1,
                User = new CustoemUserAuth
                {
                    Id = 1,
                    Email = "test1@test.com",
                    UserName = "test1",
                    PersonalId = "1234567891",
                    Roles = employeeRoles
                },
                Reviewer = new CustoemUserAuth
                {
                    Id = 2,
                    Email = "test@test.com",
                    UserName = "test",
                    PersonalId = "1234567890",
                    Roles = mangerRoles
                },
            };

        }

        public List<UserTimeOffs> GetUserTimeOffsByUserId(int userId)
        {
            var employeeRoles = new List<string>();
            employeeRoles.Add("Employee");
            var mangerRoles = new List<string>();
            mangerRoles.Add("Maneger");
            var userTimeOffs = new List<UserTimeOffs>();
            userTimeOffs.Add(new UserTimeOffs
            {
                Id = 1,
                Reason = "I want to visit my uncle",
                StatusId = 0,
                TypeId = 0,
                EndDate = new DateTime(2020, 1, 22),
                StartDate = new DateTime(2020, 1, 21),
                ReviewerId = 2,
                UserId = 1,
                User = new CustoemUserAuth
                {
                    Id = 1,
                    Email = "test1@test.com",
                    UserName = "test1",
                    PersonalId = "1234567891",
                    Roles = employeeRoles
                },
                Reviewer = new CustoemUserAuth
                {
                    Id = 2,
                    Email = "test@test.com",
                    UserName = "test",
                    PersonalId = "1234567890",
                    Roles = mangerRoles
                },
            });
            userTimeOffs.Add(new UserTimeOffs
            {
                Id = 2,
                Reason = "I want to take the reset",
                StatusId = 1,
                TypeId = 1,
                EndDate = new DateTime(2020, 1, 24),
                StartDate = new DateTime(2020, 1, 22),
                ReviewerId = 3,
                UserId = 1,
                User = new CustoemUserAuth
                {
                    Id = 1,
                    Email = "test1@test.com",
                    UserName = "test1",
                    PersonalId = "1234567891",
                    Roles = employeeRoles
                },
                Reviewer = new CustoemUserAuth
                {
                    Id = 3,
                    Email = "test@test.com",
                    UserName = "test",
                    PersonalId = "1234567892",
                    Roles = mangerRoles
                },
            });
            return userTimeOffs;
        }

        public void ReapplyUserTimeOff(int id, string message, int currentUserId)
        {
            
        }

        public void UpdateUserTimeOff(UserTimeOffs userTimeOff, int currentUserId)
        {
            
        }
    }
}
