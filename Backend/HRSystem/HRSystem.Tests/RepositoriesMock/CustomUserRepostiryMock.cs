﻿using HRSystem.ServiceInterface.Interfaces;
using HRSystem.ServiceModel;
using ServiceStack;
using ServiceStack.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.Tests.MockRepostirys
{
    public class CustomUserRepostiryMock : ICustomUserRepostiry
    {
        public void ActiveUser(int id, IRequest request)
        {
        }

        public void ChangePassword(string oldPassword, string newPassword, IRequest request)
        {

        }

        public void CreateUser(CustoemUserAuth userAuth, string password)
        {
            if (userAuth == null)
            {
                throw new System.Exception();
            }
        }

        public void DeactiveUser(int id, IRequest request)
        {

        }

        public void DeleteUser(int id, IRequest request)
        {
            if (id != 2)
            {
                throw new System.Exception();
            }
        }

        public CustoemUserAuth GetUserById(int id)
        {
            var Roles = new List<string>();
            Roles.Add("Employee");
            return new CustoemUserAuth
            {
                Id = 2,
                Email = "test@test.com",
                UserName = "test",
                PersonalId = "1234567891",
                Roles = Roles
            };
        }

        public List<CustoemUserAuth> GetUsers()
        {
            var Roles = new List<string>();
            var users = new List<CustoemUserAuth>();
            Roles.Add("Employee");
            users.Add(new CustoemUserAuth
            {
                Id = 2,
                Email = "test@test.com",
                UserName = "test",
                PersonalId = "1234567892",
                Roles = Roles
            });

            users.Add(new CustoemUserAuth
            {
                Id = 1,
                Email = "test1@test.com",
                UserName = "test1",
                PersonalId = "1234567891",
                Roles = Roles
            });
            return users;

        }

        public void ResetPassword(int id, string password)
        {
        }

        public void ShiftDaysOffServices()
        {
        }

        public CustoemUserAuth UpdateUser(UpdateUser updateUser, IRequest request)
        {
            CustoemUserAuth custoemUserAuth = updateUser.ConvertTo<CustoemUserAuth>();
            return custoemUserAuth;
        }
    }
}
